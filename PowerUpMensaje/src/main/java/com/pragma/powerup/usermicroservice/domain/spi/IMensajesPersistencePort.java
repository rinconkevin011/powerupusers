package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.domain.model.Mensaje;


public interface IMensajesPersistencePort {
    void sendMensajes(Mensaje mensaje);
}
