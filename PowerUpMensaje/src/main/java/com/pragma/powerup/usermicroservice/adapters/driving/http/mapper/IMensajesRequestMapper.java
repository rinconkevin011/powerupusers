package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.MensajePreparacionRequestDto;
import com.pragma.powerup.usermicroservice.domain.model.Mensaje;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IMensajesRequestMapper {
    Mensaje toUser(MensajePreparacionRequestDto mensajePreparacionRequestDto);
}
