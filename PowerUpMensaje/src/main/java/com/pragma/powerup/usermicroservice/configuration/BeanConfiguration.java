package com.pragma.powerup.usermicroservice.configuration;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter.MensajesMysqlAdapter;

import com.pragma.powerup.usermicroservice.domain.api.IMensajesServicePort;
import com.pragma.powerup.usermicroservice.domain.spi.IMensajesPersistencePort;
import com.pragma.powerup.usermicroservice.domain.usecase.MensajesUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    @Bean
    public IMensajesServicePort userServicePort() {
        return new MensajesUseCase(mensajesPersistencePort());
    }
    @Bean
    public IMensajesPersistencePort mensajesPersistencePort() {
        return new MensajesMysqlAdapter();
    }

}
