package com.pragma.powerup.usermicroservice.domain.model;

public class Mensaje {
    private String User;
    private String Menssage;
    private String Phone;

    public Mensaje(String user, String menssage, String phone) {
        User = user;
        Menssage = menssage;
        Phone = phone;
    }
    public Mensaje(){

    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getMenssage() {
        return Menssage;
    }

    public void setMenssage(String menssage) {
        Menssage = menssage;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }
}
