package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.domain.model.Mensaje;
import com.pragma.powerup.usermicroservice.domain.spi.IMensajesPersistencePort;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;

public class MensajesMysqlAdapter implements IMensajesPersistencePort {
    private static final String ACCOUNT_SID = "ACbb2b708aaa5f98b7ab344e9182514c90";
    private static final String AUTH_TOKEN = "7c1c8bfb2308f1ce311acb41d82ff65a";
    private static final String PHONE = "+13614188896";

    @Override
    public void sendMensajes(Mensaje mensaje) {
        Twilio.init(ACCOUNT_SID,AUTH_TOKEN);
        if(mensaje.getMenssage().equals("preparacion")){
            Message mensaje2 = Message.creator(
                    new com.twilio.type.PhoneNumber(mensaje.getPhone()),
                    new com.twilio.type.PhoneNumber(PHONE),
                    "Hi "+mensaje.getUser()+", your order is preparation.").create();


        }else if(mensaje.getMenssage().equals("finalizado")){
            Message mensaje2 = Message.creator(
                    new com.twilio.type.PhoneNumber(mensaje.getPhone()),
                    new com.twilio.type.PhoneNumber(PHONE),
                    "Hi "+mensaje.getUser()+", your order is finished.").create();


        }

    }
}
