package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity;

public class Empleado {
    private Long id;
    private Long id_client;
    private Long id_restaurante;

    public Empleado(Long id, Long id_client, Long id_restaurante) {
        this.id = id;
        this.id_client = id_client;
        this.id_restaurante = id_restaurante;
    }
    public Empleado(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_client() {
        return id_client;
    }

    public void setId_client(Long id_client) {
        this.id_client = id_client;
    }

    public Long getId_restaurante() {
        return id_restaurante;
    }

    public void setId_restaurante(Long id_restaurante) {
        this.id_restaurante = id_restaurante;
    }
}
