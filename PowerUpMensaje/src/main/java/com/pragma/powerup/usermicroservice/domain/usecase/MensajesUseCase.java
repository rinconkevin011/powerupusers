package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.api.IMensajesServicePort;
import com.pragma.powerup.usermicroservice.domain.model.Mensaje;
import com.pragma.powerup.usermicroservice.domain.spi.IMensajesPersistencePort;

public class MensajesUseCase implements IMensajesServicePort {
    private final IMensajesPersistencePort mensajesPersistencePort;

    public MensajesUseCase(IMensajesPersistencePort mensajesPersistencePort) {
        this.mensajesPersistencePort = mensajesPersistencePort;
    }

    @Override
    public void sendMenssage(Mensaje mensaje) {
        mensajesPersistencePort.sendMensajes(mensaje);
    }
}
