package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MensajePreparacionRequestDto {
    private String user;
    private String menssage;
    private String phone;
}
