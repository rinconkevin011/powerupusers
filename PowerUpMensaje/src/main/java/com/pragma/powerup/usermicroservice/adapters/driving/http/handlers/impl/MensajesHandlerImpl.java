package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.MensajePreparacionRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IMensajesHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IMensajesRequestMapper;
import com.pragma.powerup.usermicroservice.domain.api.IMensajesServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MensajesHandlerImpl implements IMensajesHandler {
    private final IMensajesServicePort mensajesServicePort;
    private final IMensajesRequestMapper mensajesRequestMapper;
    @Override
    public void sendMenssage(MensajePreparacionRequestDto mensajePreparacionRequestDto) {
        mensajesServicePort.sendMenssage(mensajesRequestMapper.toUser(mensajePreparacionRequestDto));
    }
}
