CREATE DATABASE  `db_powerup3`;
use `db_powerup3`;
--
-- Table structure for table `role`
--
CREATE TABLE if not exists`role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
--
-- Dumping data for table `role`
--
INSERT INTO `role` VALUES (1,'ROL_ADMIN','ROL_ADMIN'),(2,'ROL_PROPIETARIO','ROL_PROPIETARIO'),(3,'ROL_EMPLEADO','ROL_EMPLEADO'),(4,'ROL_CLIENTE','ROL_CLIENTE');

CREATE TABLE if not exists `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(13) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `iddnitype` varchar(50) DEFAULT NULL,
  `dninumber` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `birtdate` date DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `id_role` int DEFAULT NULL,
  `token_password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`id_role`) REFERENCES `role` (`id`)
) ;
--
-- Dumping data for table `user`
--
INSERT INTO `user` VALUES (1,'kevin','rincon','cc','1001167596','+573165692168','2000-11-11','cra 16b n 1-2','kevin@gmail.com','$2a$10$GlsGSNhkbVon6ZOSNMptOu5RikedRzlCAhMa7YpwvUSS0c88WT99S',1,NULL);

CREATE TABLE if not exists `empleados` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int DEFAULT NULL,
  `id_restaurante` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ;