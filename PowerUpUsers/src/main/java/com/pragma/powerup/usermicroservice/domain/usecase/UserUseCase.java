package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.InvalidPhone;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.NoMeetAgeException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.PersonAlreadyExistsException;
import com.pragma.powerup.usermicroservice.domain.api.IUserServicePort;
import com.pragma.powerup.usermicroservice.domain.model.Empleado;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.model.UserEmployee;
import com.pragma.powerup.usermicroservice.domain.spi.IUserPersistencePort;

import javax.xml.crypto.Data;
import java.util.Calendar;
import java.util.List;

import static com.pragma.powerup.usermicroservice.configuration.Constants.*;

public class UserUseCase implements IUserServicePort {
    private final IUserPersistencePort userPersistencePort;

    public UserUseCase(IUserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }

    @Override
    public void saveUser(User user) {
        if(!validateDate(user.getBirtdate())){
            throw new NoMeetAgeException();
        }
        if(user.getPhone().length() != 10){
            throw new InvalidPhone();
        }
        user.setPhone("+57"+user.getPhone());
        user.setId_role(OWNER_ROLE_ID);

        userPersistencePort.saveUser(user);
    }
    @Override
    public void saveUserEmployee(UserEmployee user) {
        if(!validateDate(user.getBirtdate())){
            throw new NoMeetAgeException();
        }
        if(user.getPhone().length() != 10){
            throw new InvalidPhone();
        }
        user.setPhone("+57"+user.getPhone());
        user.setId_role(EMPLOYEE_ROLE_ID);
        userPersistencePort.saveUserEmployee(user);
    }
    @Override
    public void saveUserClient(User user) {
        if(!validateDate(user.getBirtdate())){
            throw new NoMeetAgeException();
        }
        if(user.getPhone().length() != 10){
            throw new InvalidPhone();
        }
        user.setPhone("+57"+user.getPhone());
        user.setId_role(CLIENT_ROLE_ID);
        userPersistencePort.saveUserClient(user);
    }

    @Override
    public User getClient(String id) {
        return userPersistencePort.getClient(id);
    }

    @Override
    public User getClientid(Long id) {
        return userPersistencePort.getClientid(id);
    }

    @Override
    public List<Empleado> getAllEmployee() {
        return userPersistencePort.getAllEmployee();
    }

    @Override
    public Empleado getEmployeeid(Long id) {
        return userPersistencePort.getEmployeeid(id);
    }

    public Boolean validateDate(String data){
        Calendar c = Calendar.getInstance();
        String dia, mes, año;

        dia = Integer.toString(c.get(Calendar.DATE));
        mes = Integer.toString(c.get(Calendar.MONTH));
        año = Integer.toString(c.get(Calendar.YEAR));

        String[] data1 = data.split("-");
        if((Integer.parseInt(año) - Integer.parseInt(data1[0]) > 18)){}
        else if((Integer.parseInt(año) - Integer.parseInt(data1[0]) == 18)){
            if(Integer.parseInt(mes) > Integer.parseInt(data1[1])){}
            else if(Integer.parseInt(mes) == Integer.parseInt(data1[1])){
                if(Integer.parseInt(dia) < Integer.parseInt(data1[2])){
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

        return true;
    }
}
