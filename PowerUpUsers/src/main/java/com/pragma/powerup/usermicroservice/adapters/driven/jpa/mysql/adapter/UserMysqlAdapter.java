package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;


import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.EmpleadoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.*;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IEmpleadoEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IEmpleadoRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IRoleRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IUserRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IUserEntityMapper;
import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtProvider;
import com.pragma.powerup.usermicroservice.domain.model.Empleado;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.model.UserEmployee;
import com.pragma.powerup.usermicroservice.domain.spi.IUserPersistencePort;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import static com.pragma.powerup.usermicroservice.configuration.Constants.*;

@RequiredArgsConstructor
@Transactional
public class UserMysqlAdapter implements IUserPersistencePort {
    private final IUserRepository userRepository;
    private final IRoleRepository roleRepository;
    private final IUserEntityMapper userEntityMapper;
    private final PasswordEncoder passwordEncoder;
    private final JwtProvider jwtProvider;
    private final IEmpleadoRepository empleadoRepository;
    private final IEmpleadoEntityMapper empleadoEntityMapper;

    @Override
    public void saveUser(User user) {
        if (userRepository.existsByMail(user.getMail())){
            throw new MailAlreadyExistsException();
        }
        if (userRepository.findByDninumber(user.getDninumber()).isPresent()) {
            throw new PersonAlreadyExistsException();
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        userRepository.save(userEntityMapper.toEntity(user));
    }

    @Override
    public void saveUserEmployee(UserEmployee user2) {
        User user = new User(user2.getName(), user2.getSurname(), user2.getIddnitype(), user2.getDninumber(), user2.getPhone(), user2.getBirtdate(), user2.getAddress(), user2.getMail(), user2.getPassword(), user2.getId_role());

        Optional<UserEntity> log;
        String DNI = getDniForToken();

        log = userRepository.findByDninumber(DNI);

        ResponseEntity<Restaurante> responseRestaurant = GetRestaurantId(user2.getIdrestaurant());

        if(responseRestaurant.getBody().getIduserPr() != log.get().getId()){
            throw new NotOwnerThisRestaurant();
        }
        if (userRepository.existsByMail(user.getMail())){
            throw new MailAlreadyExistsException();
        }
        if (userRepository.findByDninumber(user.getDninumber()).isPresent()) {
            throw new PersonAlreadyExistsException();
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(userEntityMapper.toEntity(user));

        List<UserEntity> listU = userRepository.findAll();
        Long p = listU.get(listU.size()-1).getId();
        Empleado emp = new Empleado();

        emp.setId_user(p);
        emp.setId_restaurante(user2.getIdrestaurant());

        empleadoRepository.save(empleadoEntityMapper.toEntity(emp));

    }
    @Override
    public void saveUserClient(User user) {
        if (userRepository.existsByMail(user.getMail())){
            throw new MailAlreadyExistsException();
        }
        if (userRepository.findByDninumber(user.getDninumber()).isPresent()) {
            throw new PersonAlreadyExistsException();
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        userRepository.save(userEntityMapper.toEntity(user));
    }

    @Override
    public User getClient(String dninumber) {
        UserEntity userEntity = userRepository.findByDninumber(dninumber).orElseThrow(UserNotFoundException::new);
        return userEntityMapper.toUser(userEntity);
    }

    @Override
    public User getClientid(Long id) {
        UserEntity userEntity = userRepository.findByid(id).orElseThrow(UserNotFoundException::new);
        return userEntityMapper.toUser(userEntity);
    }

    @Override
    public List<Empleado> getAllEmployee() {
        List<EmpleadoEntity> empleadoEntities2 = new ArrayList<>();
        List<EmpleadoEntity> empleadoEntities = empleadoRepository.findAll();

        if (empleadoEntities.isEmpty()) {
            throw new NoDataFoundException();
        }

        Optional<UserEntity> log;
        String DNI = getDniForToken();

        log = userRepository.findByDninumber(DNI);

        for (EmpleadoEntity empleado : empleadoEntities){
            ResponseEntity<Restaurante> responseRestaurant = GetRestaurantId(empleado.getId_restaurante());
            if(log.get().getId().equals(responseRestaurant.getBody().getIduserPr())){
                empleadoEntities2.add(empleado);
            }
        }

        return empleadoEntityMapper.toEmpleadoList(empleadoEntities2);
    }

    @Override
    public Empleado getEmployeeid(Long id) {
        EmpleadoEntity empleado = empleadoRepository.findByiduser(id);
        return empleadoEntityMapper.toUser(empleado);
    }
    public String getDniForToken(){
        File archivo = new File("token");
        String dni = "";

        try {
            //traer token
            BufferedReader entrada = new BufferedReader(new FileReader(archivo));
            String token = entrada.readLine();
            dni = jwtProvider.getNombreUsuarioFromToken(token);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        archivo.delete();
        return dni;
    }
    public ResponseEntity<Restaurante> GetRestaurantId(Long idRestaurant){
        String theUrl2 = "http://localhost:8091/restaurante/GetRestaurantId/"+idRestaurant;
        RestTemplate restTemplate2 = new RestTemplate();
        HttpHeaders headers2 = new HttpHeaders();
        headers2.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity2 = new HttpEntity<String>("parameters", headers2);
        ResponseEntity<Restaurante> responseRestaurant = restTemplate2.exchange(theUrl2, HttpMethod.GET, entity2, Restaurante.class);

        return responseRestaurant;
    }


}
