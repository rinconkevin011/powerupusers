package com.pragma.powerup.usermicroservice.domain.model;

public class Empleado {
    private Long id;
    private Long id_user;
    private Long id_restaurante;

    public Empleado(Long id, Long id_user, Long id_restaurante) {
        this.id = id;
        this.id_user = id_user;
        this.id_restaurante = id_restaurante;
    }
    public Empleado(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Long getId_restaurante() {
        return id_restaurante;
    }

    public void setId_restaurante(Long id_restaurante) {
        this.id_restaurante = id_restaurante;
    }
}
