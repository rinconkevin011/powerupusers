package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserEmployeeRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.EmpleadoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IUserRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IUserResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.IUserServicePort;
import com.pragma.powerup.usermicroservice.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class UserHandlerImpl implements IUserHandler {
    private final IUserServicePort userServicePort;
    private final IUserRequestMapper userRequestMapper;
    private final IUserResponseMapper userResponseMapper;

    @Override
    public void saveUser(UserRequestDto userRequestDto) {
        userServicePort.saveUser(userRequestMapper.toUser(userRequestDto));
    }

    @Override
    public void saveUserClient(UserRequestDto userRequestDto) {
        userServicePort.saveUserClient(userRequestMapper.toUser(userRequestDto));
    }

    @Override
    public void saveUserEmployee(UserEmployeeRequestDto userEmployeeRequestDto) {
        userServicePort.saveUserEmployee(userRequestMapper.toUser2(userEmployeeRequestDto));
    }

    @Override
    public UserResponseDto getClient(String id) {
        return userResponseMapper.userToPersonResponse(userServicePort.getClient(id));
    }

    @Override
    public UserResponseDto getClientid(Long id) {
        return userResponseMapper.userToPersonResponse(userServicePort.getClientid(id));
    }

    @Override
    public List<EmpleadoResponseDto> getAllEmployee() {
        return userResponseMapper.toResponseList(userServicePort.getAllEmployee());
    }

    @Override
    public EmpleadoResponseDto getEmployeeid(Long id) {
        return userResponseMapper.userToEmployeeResponse(userServicePort.getEmployeeid(id));
    }

}
