package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserEmployeeRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.EmpleadoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RoleResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.User;

import java.util.List;

public interface IUserHandler {
    void saveUser(UserRequestDto userRequestDto);
    void saveUserClient(UserRequestDto userRequestDto);
    void saveUserEmployee(UserEmployeeRequestDto userEmployeeRequestDto);
    UserResponseDto getClient(String dninumber);
    UserResponseDto getClientid(Long id);
    List<EmpleadoResponseDto> getAllEmployee();
    EmpleadoResponseDto getEmployeeid(Long id);
}
