package com.pragma.powerup.usermicroservice.domain.model;

public class UserEmployee {
    private  Long id;
    private String name;
    private String surname;
    private String iddnitype;
    private String dninumber;
    private String phone;
    private String birtdate;
    private String address;
    private String mail;
    private String password;
    private Long id_role;
    private Long idrestaurant;

    public UserEmployee( String name, String surname, String iddnitype, String dninumber, String phone, String birtdate, String address, String mail, String password, Long id_role, Long idrestaurant) {
        this.name = name;
        this.surname = surname;
        this.iddnitype = iddnitype;
        this.dninumber = dninumber;
        this.phone = phone;
        this.birtdate = birtdate;
        this.address = address;
        this.mail = mail;
        this.password = password;
        this.id_role = id_role;
        this.idrestaurant = idrestaurant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIddnitype() {
        return iddnitype;
    }

    public void setIddnitype(String iddnitype) {
        this.iddnitype = iddnitype;
    }

    public String getDninumber() {
        return dninumber;
    }

    public void setDninumber(String dninumber) {
        this.dninumber = dninumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirtdate() {
        return birtdate;
    }

    public void setBirtdate(String birtdate) {
        this.birtdate = birtdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId_role() {
        return id_role;
    }

    public void setId_role(Long id_role) {
        this.id_role = id_role;
    }

    public Long getIdrestaurant() {
        return idrestaurant;
    }

    public void setIdrestaurant(Long idrestaurant) {
        this.idrestaurant = idrestaurant;
    }
}
