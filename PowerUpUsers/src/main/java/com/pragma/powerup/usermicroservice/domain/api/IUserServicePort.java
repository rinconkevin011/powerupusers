package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.domain.model.Empleado;
import com.pragma.powerup.usermicroservice.domain.model.Role;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.model.UserEmployee;

import java.util.List;

public interface IUserServicePort {
    void saveUser(User user);
    void saveUserClient(User user);
    void saveUserEmployee(UserEmployee user);
    User getClient(String id);
    User getClientid(Long id);
    List<Empleado> getAllEmployee();
    Empleado getEmployeeid(Long id);
}
