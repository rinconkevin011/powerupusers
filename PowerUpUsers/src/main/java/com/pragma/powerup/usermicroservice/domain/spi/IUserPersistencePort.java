package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.domain.model.Empleado;
import com.pragma.powerup.usermicroservice.domain.model.Role;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.model.UserEmployee;

import java.util.List;

public interface IUserPersistencePort {
    void saveUser(User user);
    void saveUserEmployee(UserEmployee user);

    void saveUserClient(User user);

    User getClient(String id);

    User getClientid(Long id);

    List<Empleado> getAllEmployee();

    Empleado getEmployeeid(Long id);
}
