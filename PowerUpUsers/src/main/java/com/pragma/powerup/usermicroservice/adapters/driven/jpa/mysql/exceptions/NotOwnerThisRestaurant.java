package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class NotOwnerThisRestaurant extends RuntimeException{
    public NotOwnerThisRestaurant(){
        super();
    }
}
