package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.EmpleadoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RoleResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.Empleado;
import com.pragma.powerup.usermicroservice.domain.model.Role;
import com.pragma.powerup.usermicroservice.domain.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IUserResponseMapper {

    @Mapping(source = "user.name", target = "name")
    @Mapping(source = "user.surname", target = "surname")
    @Mapping(source = "user.iddnitype", target = "iddnitype")
    @Mapping(source = "user.dninumber", target = "dninumber")
    @Mapping(source = "user.phone", target = "phone")
    @Mapping(source = "user.birtdate", target = "birtdate")
    @Mapping(source = "user.address", target = "address")
    @Mapping(source = "user.mail", target = "mail")
    @Mapping(source = "user.password", target = "password")
    @Mapping(target = "id_role", source = "id_role")
    UserResponseDto userToPersonResponse(User user);
    EmpleadoResponseDto userToEmployeeResponse(Empleado user);
    List<EmpleadoResponseDto> toResponseList(List<Empleado> roleList);


}
