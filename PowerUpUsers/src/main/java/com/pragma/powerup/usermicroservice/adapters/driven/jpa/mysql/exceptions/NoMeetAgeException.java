package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class NoMeetAgeException extends RuntimeException{
    public NoMeetAgeException(){
        super();
    }
}
