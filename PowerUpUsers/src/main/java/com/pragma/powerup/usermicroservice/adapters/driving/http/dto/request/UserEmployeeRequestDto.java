package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserEmployeeRequestDto {
    //private Long id;
    private String name;
    private String surname;
    private String iddnitype;
    private String dninumber;
    private String phone;
    private String birtdate;
    private String address;
    private String mail;
    private String password;
    private Long idrestaurant;

}
