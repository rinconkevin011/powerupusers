package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PrincipalUser;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IRoleEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IUserEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IUserRepository;
import com.pragma.powerup.usermicroservice.domain.model.Role;
import com.pragma.powerup.usermicroservice.domain.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class UserDetailsServiceImplTest {
    @Mock
    private IUserRepository userRepository;
    @Mock
    private IUserEntityMapper userEntityMapper;
    @Mock
    private IRoleEntityMapper roleEntityMapper;
    private Role role1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String description = "hksdhfkas";
    private User user1;
    private static final Long idU = Long.valueOf(15);
    private static final String nameU = "dsafs";
    private static final String surname = "hksdhfkas";
    private static final String iddnitype = "cc";
    private static final String dninumber = "1001167598";
    private static final String phone = "3165692167";
    private static final String birtdate = "2000-02-02";
    private static final String address = "cra 15 n 8 - 47";
    private static final String mail = "fdsafas@gmail.com";
    private static final String password = "1234";
    private static final Long id_role = Long.valueOf(2);

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        role1 = Mockito.mock(Role.class);
        when(role1.getId()).thenReturn(id);
        when(role1.getName()).thenReturn(name);
        when(role1.getDescription()).thenReturn(description);

        user1 = Mockito.mock(User.class);
        when(user1.getId()).thenReturn(idU);
        when(user1.getName()).thenReturn(nameU);
        when(user1.getSurname()).thenReturn(surname);
        when(user1.getIddnitype()).thenReturn(iddnitype);
        when(user1.getDninumber()).thenReturn(dninumber);
        when(user1.getPhone()).thenReturn(phone);
        when(user1.getBirtdate()).thenReturn(birtdate);
        when(user1.getAddress()).thenReturn(address);
        when(user1.getMail()).thenReturn(mail);
        when(user1.getPassword()).thenReturn(password);
        when(user1.getId_role()).thenReturn(id_role);
    }

    @Test
    void loadUserByUsername() {
        /**List<Role> roles = new ArrayList();
        roles.add(role1);
        PrincipalUser.build(userEntityMapper.toEntity(user1),roleEntityMapper.toRoleList(roles));*/


    }
}