package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.domain.model.Role;
import com.pragma.powerup.usermicroservice.domain.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class IUserRepositoryTest {

    @Mock
    IUserRepository userRepository;
    private  User user1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String surname = "hksdhfkas";
    private static final String iddnitype = "cc";
    private static final String dninumber = "1001167598";
    private static final String phone = "3165692167";
    private static final String birtdate = "2000-02-02";
    private static final String address = "cra 15 n 8 - 47";
    private static final String mail = "fdsafas@gmail.com";
    private static final String password = "1234";
    private static final Long id_role = Long.valueOf(2);

    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        user1 = Mockito.mock(User.class);
        when(user1.getId()).thenReturn(id);
        when(user1.getName()).thenReturn(name);
        when(user1.getSurname()).thenReturn(surname);
        when(user1.getIddnitype()).thenReturn(iddnitype);
        when(user1.getDninumber()).thenReturn(dninumber);
        when(user1.getPhone()).thenReturn(phone);
        when(user1.getBirtdate()).thenReturn(birtdate);
        when(user1.getAddress()).thenReturn(address);
        when(user1.getMail()).thenReturn(mail);
        when(user1.getPassword()).thenReturn(password);
        when(user1.getId_role()).thenReturn(id_role);


    }

    @Test
    void findByDninumber() {
        userRepository.findByDninumber(dninumber);
        verify(userRepository).findByDninumber(dninumber);

    }

    @Test
    void findByid() {
        userRepository.findByid(id);
        verify(userRepository).findByid(id);

    }

    @Test
    void existsByMail() {
        when(userRepository.existsByMail(mail)).thenReturn(true);
        assertNotNull(userRepository.findByDninumber(mail));

    }
}