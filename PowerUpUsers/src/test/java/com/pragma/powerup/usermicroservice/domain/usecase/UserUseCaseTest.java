package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.model.Empleado;
import com.pragma.powerup.usermicroservice.domain.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserUseCaseTest {
    @Mock
    UserUseCase userUseCase;
    private User user1;
    private  Empleado empleado1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String surname = "hksdhfkas";
    private static final String iddnitype = "cc";
    private static final String dninumber = "1001167598";
    private static final String phone = "3165692167";
    private static final String birtdate = "2000-02-02";
    private static final String address = "cra 15 n 8 - 47";
    private static final String mail = "fdsafas@gmail.com";
    private static final String password = "1234";
    private static final Long id_role = Long.valueOf(2);
    private static final Long ide = Long.valueOf(2);
    private static final Long id1_usere = Long.valueOf(2);
    private static final Long id_restaurante = Long.valueOf(2);

    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        user1 = Mockito.mock(User.class);
        when(user1.getId()).thenReturn(id);
        when(user1.getName()).thenReturn(name);
        when(user1.getSurname()).thenReturn(surname);
        when(user1.getIddnitype()).thenReturn(iddnitype);
        when(user1.getDninumber()).thenReturn(dninumber);
        when(user1.getPhone()).thenReturn(phone);
        when(user1.getBirtdate()).thenReturn(birtdate);
        when(user1.getAddress()).thenReturn(address);
        when(user1.getMail()).thenReturn(mail);
        when(user1.getPassword()).thenReturn(password);
        when(user1.getId_role()).thenReturn(id_role);

        empleado1 = Mockito.mock(Empleado.class);
        when(empleado1.getId()).thenReturn(ide);
        when(empleado1.getId_user()).thenReturn(id1_usere);
        when(empleado1.getId_restaurante()).thenReturn(id_restaurante);

    }
    @Test
    void saveUser() {
        userUseCase.saveUser(user1);
        verify(userUseCase).saveUser(user1);
    }

    @Test
    void getClient() {
        when(userUseCase.getClient(dninumber)).thenReturn(user1);
        assertNotNull(userUseCase.getClient(dninumber));
    }

    @Test
    void getClientid() {
        when(userUseCase.getClientid(id)).thenReturn(user1);
        assertNotNull(userUseCase.getClientid(id));
    }

    @Test
    void getAllRoles() {
        when(userUseCase.getAllRoles()).thenReturn(Arrays.asList(empleado1) );
    }

    @Test
    void getEmployeeid() {
        when(userUseCase.getClientid(ide)).thenReturn(user1);
    }
}