package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.domain.model.Empleado;
import com.pragma.powerup.usermicroservice.domain.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class IEmpleadoRepositoryTest {
    @Mock
    IEmpleadoRepository empleadoRepository;
    private Empleado empleado1;
    private static final Long id = Long.valueOf(15);
    private static final Long id_user = Long.valueOf(2);
    private static final Long id_restaurante = Long.valueOf(2);

    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        empleado1 = Mockito.mock(Empleado.class);
        when(empleado1.getId()).thenReturn(id);
        when(empleado1.getId_user()).thenReturn(id_user);
        when(empleado1.getId_restaurante()).thenReturn(id_restaurante);



    }
    @Test
    void findByiduser() {
        empleadoRepository.findByiduser(id_user);
        verify(empleadoRepository).findByiduser(id_user);
    }
}