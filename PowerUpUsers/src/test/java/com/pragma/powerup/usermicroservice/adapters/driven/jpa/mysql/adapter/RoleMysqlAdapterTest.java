package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.NoDataFoundException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IRoleEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IRoleRepository;
import com.pragma.powerup.usermicroservice.domain.model.Role;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.pragma.powerup.usermicroservice.domain.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RoleMysqlAdapterTest {
    @Mock
    private RoleMysqlAdapter rolemysqladapter;
    @Mock
    private  IRoleRepository roleRepository;
    @Mock
    private  IRoleEntityMapper roleEntityMapper;

    private  List<Role> roles = new ArrayList<>();

    private Role role1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String description = "hksdhfkas";

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        role1 = Mockito.mock(Role.class);

        when(role1.getId()).thenReturn(id);
        when(role1.getName()).thenReturn(name);
        when(role1.getDescription()).thenReturn(description);
    }

    @Test
    void getAllRoles() {
        when(rolemysqladapter.getAllRoles()).thenReturn(Arrays.asList(role1));
        assertNotNull(roleRepository.findAll());

    }
}