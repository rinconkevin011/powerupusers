package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IUserEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IEmpleadoRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IUserRepository;
import com.pragma.powerup.usermicroservice.domain.model.Empleado;
import com.pragma.powerup.usermicroservice.domain.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.InjectMocks.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
class UserMysqlAdapterTest {
    @Mock
    UserMysqlAdapter userMysqlAdapter;
    @Mock
    private IUserRepository userRepository;
    @Mock
    private IUserEntityMapper userEntityMapper;
    @Mock
    private IEmpleadoRepository empleadoRepository;

    private  User user1;
    private  Empleado empleado1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String surname = "hksdhfkas";
    private static final String iddnitype = "cc";
    private static final String dninumber = "1001167598";
    private static final String phone = "3165692167";
    private static final String birtdate = "2000-02-02";
    private static final String address = "cra 15 n 8 - 47";
    private static final String mail = "fdsafas@gmail.com";
    private static final String password = "1234";
    private static final Long id_role = Long.valueOf(2);
    private static final Long ide = Long.valueOf(2);
    private static final Long id1_usere = Long.valueOf(2);
    private static final Long id_restaurante = Long.valueOf(2);



    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        user1 = Mockito.mock(User.class);
        when(user1.getId()).thenReturn(id);
        when(user1.getName()).thenReturn(name);
        when(user1.getSurname()).thenReturn(surname);
        when(user1.getIddnitype()).thenReturn(iddnitype);
        when(user1.getDninumber()).thenReturn(dninumber);
        when(user1.getPhone()).thenReturn(phone);
        when(user1.getBirtdate()).thenReturn(birtdate);
        when(user1.getAddress()).thenReturn(address);
        when(user1.getMail()).thenReturn(mail);
        when(user1.getPassword()).thenReturn(password);
        when(user1.getId_role()).thenReturn(id_role);

        empleado1 = Mockito.mock(Empleado.class);
        when(empleado1.getId()).thenReturn(ide);
        when(empleado1.getId_user()).thenReturn(id1_usere);
        when(empleado1.getId_restaurante()).thenReturn(id_restaurante);

    }


    @Test
    void saveUser() {
        userMysqlAdapter.saveUser(user1);
        verify(userMysqlAdapter).saveUser(user1);
    }

    @Test
    void getClient() {
        when(userMysqlAdapter.getClient(dninumber)).thenReturn(user1);
        assertNotNull(userRepository.findByDninumber(dninumber));
    }

    @Test
    void getClientid() {
        when(userMysqlAdapter.getClientid(id)).thenReturn(user1);
        assertNotNull(userRepository.findByid(id));
    }

    @Test
    void getAllRoles() {
        when(userMysqlAdapter.getAllRoles()).thenReturn(Arrays.asList(empleado1) );
    }

    @Test
    void getEmployeeid() {
        when(userMysqlAdapter.getClientid(ide)).thenReturn(user1);
    }
}