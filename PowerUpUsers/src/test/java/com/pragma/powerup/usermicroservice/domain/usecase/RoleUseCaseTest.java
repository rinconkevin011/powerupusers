package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.model.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class RoleUseCaseTest {
    @Mock
    RoleUseCase roleUseCase;
    private Role role1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String description = "hksdhfkas";
    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        role1 = Mockito.mock(Role.class);

        when(role1.getId()).thenReturn(id);
        when(role1.getName()).thenReturn(name);
        when(role1.getDescription()).thenReturn(description);
    }
    @Test
    void getAllRoles() {
        when(roleUseCase.getAllRoles()).thenReturn(Arrays.asList(role1));
        assertNotNull(roleUseCase.getAllRoles());
    }
}