CREATE DATABASE  IF NOT EXISTS `db_powerup2`;
USE `db_powerup2`;
--
-- Table structure for table `categoria`
--
CREATE TABLE `categoria` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
--
-- Table structure for table `restaurante`
--
CREATE TABLE `restaurante` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(13) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `urllogo` varchar(50) DEFAULT NULL,
  `nit` varchar(50) DEFAULT NULL,
  `id_user_pr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
--
-- Table structure for table `pedido`
--
CREATE TABLE `pedido` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `idrestaurante` int DEFAULT NULL,
  `idusercl` int DEFAULT NULL,
  `iduserch` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`idrestaurante`) REFERENCES `restaurante` (`id`)
);
--
-- Table structure for table `plato`
--
CREATE TABLE `plato` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(13) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `value` int DEFAULT NULL,
  `urlimagen` varchar(50) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `id_cateforia` int DEFAULT NULL,
  `idrestaurante` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`id_cateforia`) REFERENCES `categoria` (`id`),
  CONSTRAINT FOREIGN KEY (`idrestaurante`) REFERENCES `restaurante` (`id`)
);
--
-- Table structure for table `pedido_platos`
--
CREATE TABLE `pedido_platos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` varchar(50) DEFAULT NULL,
  `id_pedido` int DEFAULT NULL,
  `id_plato` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id`),
  CONSTRAINT FOREIGN KEY (`id_plato`) REFERENCES `plato` (`id`)
);
