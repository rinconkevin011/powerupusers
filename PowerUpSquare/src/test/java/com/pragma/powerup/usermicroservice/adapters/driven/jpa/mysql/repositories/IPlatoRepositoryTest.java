package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class IPlatoRepositoryTest {

    @Mock
    IPlatoRepository platoRepository;

    private Plato plato1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String description = "cra 15 n 8 - 47";
    private static final int value = 5000;
    private static final String urlimagen = "hksdhfkas";
    private static final Boolean activo = true;
    private static final Long id_categoria = Long.valueOf(2);
    private static final Long idrestaurante = Long.valueOf(1);


    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        plato1 = Mockito.mock(Plato.class);
        when(plato1.getId()).thenReturn(id);
        when(plato1.getName()).thenReturn(name);
        when(plato1.getDescription()).thenReturn(description);
        when(plato1.getValue()).thenReturn((long) value);
        when(plato1.getUrlimagen()).thenReturn(urlimagen);
        when(plato1.getActivo()).thenReturn(activo);
        when(plato1.getId_categoria()).thenReturn(Long.valueOf(id_categoria));
        when(plato1.getIdrestaurante()).thenReturn(Long.valueOf(idrestaurante));

    }

    @Test
    void existsByUrlimagen() {
        when(platoRepository.existsByUrlimagen(urlimagen)).thenReturn(true);
        assertNotNull(platoRepository.existsByUrlimagen(urlimagen));
    }

    @Test
    void findById() {
        platoRepository.findById(id);
        verify(platoRepository).findById(id);
    }
}