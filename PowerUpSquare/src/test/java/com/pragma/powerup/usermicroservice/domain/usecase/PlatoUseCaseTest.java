package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.model.Plato;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PlatoUseCaseTest {
    @Mock
    PlatoUseCase platoUseCase;
    private Plato plato1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String description = "cra 15 n 8 - 47";
    private static final int value = 5000;
    private static final String urlimagen = "hksdhfkas";
    private static final Boolean activo = true;
    private static final Long id_categoria = Long.valueOf(2);
    private static final Long idrestaurante = Long.valueOf(1);


    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        plato1 = Mockito.mock(Plato.class);
        when(plato1.getId()).thenReturn(id);
        when(plato1.getName()).thenReturn(name);
        when(plato1.getDescription()).thenReturn(description);
        when(plato1.getValue()).thenReturn((long) value);
        when(plato1.getUrlimagen()).thenReturn(urlimagen);
        when(plato1.getActivo()).thenReturn(activo);
        when(plato1.getId_categoria()).thenReturn(Long.valueOf(id_categoria));
        when(plato1.getIdrestaurante()).thenReturn(Long.valueOf(idrestaurante));
    }

    @Test
    void savePlato() {
        platoUseCase.savePlato(plato1);
        verify(platoUseCase).savePlato(plato1);
    }

    @Test
    void updatePlato() {
        platoUseCase.updatePlato(plato1);
        verify(platoUseCase).updatePlato(plato1);
    }

    @Test
    void enablePlato() {

    }

    @Test
    void getPlatoidrestaurante() {
        when(platoUseCase.getPlatoidrestaurante(plato1.getIdrestaurante())).thenReturn(Arrays.asList(plato1));
        assertNotNull(platoUseCase.getPlatoidrestaurante(idrestaurante));
    }

    @Test
    void getPlatoid() {
        when(platoUseCase.getPlatoid(plato1.getId())).thenReturn(plato1);
        assertNotNull(platoUseCase.getPlatoid(id));
    }
}