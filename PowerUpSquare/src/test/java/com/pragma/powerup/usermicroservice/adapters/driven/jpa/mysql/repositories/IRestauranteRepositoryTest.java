package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class IRestauranteRepositoryTest {
    @Mock
    private IRestauranteRepository restauranteRepository;
    private Restaurante restaurante1;
    private static final Long id = Long.valueOf(15);
    private static final String name = "dsafs";
    private static final String address = "cra 15 n 8 - 47";
    private static final String phone = "3165692167";
    private static final String urllogo = "hksdhfkas";
    private static final String nit = "123456";
    private static final String id_user_pr = "2";


    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        restaurante1 = Mockito.mock(Restaurante.class);
        when(restaurante1.getId()).thenReturn(id);
        when(restaurante1.getName()).thenReturn(name);
        when(restaurante1.getPhone()).thenReturn(phone);
        when(restaurante1.getAddress()).thenReturn(address);
        when(restaurante1.getUrllogo()).thenReturn(urllogo);
        when(restaurante1.getNit()).thenReturn(nit);
        when(restaurante1.getIduserPr()).thenReturn(Long.valueOf(id_user_pr));

    }

    @Test
    void findByid() {
        restauranteRepository.findById(id);
        verify(restauranteRepository).findById(id);
    }

    @Test
    void findByname() {
        restauranteRepository.findByname(name);
        verify(restauranteRepository).findByname(name);
    }

    @Test
    void existsByUrllogo() {
        when(restauranteRepository.existsByUrllogo(urllogo)).thenReturn(true);
        assertNotNull(restauranteRepository.existsByUrllogo(urllogo));
    }
}