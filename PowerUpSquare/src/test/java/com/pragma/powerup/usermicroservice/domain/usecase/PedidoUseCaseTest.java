package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter.PedidoMysqlAdapter;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IPedidoEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IPedidoRepository;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PedidoUseCaseTest {
    @Mock
    PedidoMysqlAdapter pedidoMysqlAdapter;
    @Mock
    IPedidoRepository pedidoRepository;
    @Mock
    IPedidoEntityMapper pedidoEntityMapper;

    private Pedido pedido1;
    private static final Long id = Long.valueOf(15);
    private static final String description = "calientico";
    private static final String data = "2023-05-04";
    private static final Long idrestaurante = Long.valueOf(1);
    private static final Long id_userCl = Long.valueOf(2);
    private static final Long id_userCh = Long.valueOf(1);

    private Pedido_platos pedidoPlatos;
    private static final Long idp = Long.valueOf(15);
    private static final String value = "calientico";
    private static final Long id_pedido = Long.valueOf(1);
    private static final Long id_plato = Long.valueOf(2);


    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        pedido1 = Mockito.mock(Pedido.class);
        when(pedido1.getId()).thenReturn(id);
        when(pedido1.getDescription()).thenReturn(description);
        when(pedido1.getData()).thenReturn(data);
        when(pedido1.getIdrestaurante()).thenReturn(idrestaurante);
        when(pedido1.getIdusercl()).thenReturn(id_userCl);
        when(pedido1.getIduserch()).thenReturn(id_userCh);

        pedidoPlatos = Mockito.mock(Pedido_platos.class);
        when(pedidoPlatos.getId()).thenReturn(idp);
        when(pedidoPlatos.getValue()).thenReturn(value);
        when(pedidoPlatos.getId_pedido()).thenReturn(id_pedido);
        when(pedidoPlatos.getId_plato()).thenReturn(id_plato);


    }
    @Test
    void savePedido() {
        pedidoMysqlAdapter.savePedido(pedido1, pedidoPlatos);
        verify(pedidoMysqlAdapter).savePedido(pedido1, pedidoPlatos);
    }

    @Test
    void updatePedido() {
        pedidoMysqlAdapter.updatePedido(pedidoPlatos);
        verify(pedidoMysqlAdapter).updatePedido(pedidoPlatos);
    }

    @Test
    void getPedidosEstado() {
        when(pedidoMysqlAdapter.getPedidoEstado(pedidoPlatos.getValue())).thenReturn(Arrays.asList(pedido1));
        assertNotNull(pedidoMysqlAdapter.getPedidoEstado(value));
    }
}