package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.domain.model.Restaurante;

import java.util.List;

public interface IRestauranteServicePort {
    void saveRestaurante(Restaurante restaurante);
    Restaurante getrestauranteid(Long id);
    List<Restaurante> getrestauranteOwner(String iduserpr);
    List<Restaurante> getAllRestaurant();
}
