package com.pragma.powerup.usermicroservice.domain.model;

public class Plato {
    private Long id;
    private String name;
    private String description;
    private Long value;
    private String urlimagen;
    private Boolean activo;
    private Long id_categoria;
    private Long idrestaurante;

    public Plato(){

    }
    public Plato(Long id, String name, String description, Long value, String urlimagen, Boolean activo, Long id_categoria, Long idrestaurante) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.value = value;
        this.urlimagen = urlimagen;
        this.activo = activo;
        this.id_categoria = id_categoria;
        this.idrestaurante = idrestaurante;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public String getUrlimagen() {
        return urlimagen;
    }

    public void setUrlimagen(String urlimagen) {
        this.urlimagen = urlimagen;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Long getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Long id_categoria) {
        this.id_categoria = id_categoria;
    }

    public Long getIdrestaurante() {
        return idrestaurante;
    }

    public void setIdrestaurante(Long idrestaurante) {
        this.idrestaurante = idrestaurante;
    }
}
