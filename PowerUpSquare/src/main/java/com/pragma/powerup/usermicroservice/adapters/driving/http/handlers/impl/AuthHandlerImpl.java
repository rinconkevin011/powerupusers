package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.nimbusds.jose.shaded.gson.JsonObject;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.LoginRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IAuthHandler;
import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class AuthHandlerImpl implements IAuthHandler {

    //private final AuthenticationManager authenticationManager;
    //private final JwtProvider jwtProvider;
    //private final RestTemplate restTemplate;
    //private  final JwtResponseDto jwtResponseDto;
    //private final LoginRequestDto loginRequestDto;
    private final JwtProvider jwtProvider;

    @Override
    public JwtResponseDto login(LoginRequestDto loginRequestDto) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JsonObject properties = new JsonObject();
        properties.addProperty("userdni", loginRequestDto.getUserdni());
        properties.addProperty("password", loginRequestDto.getPassword());
        HttpEntity<String> request = new HttpEntity<>(properties.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        JwtResponseDto response = restTemplate.postForObject("http://localhost:8090/auth/login", request, JwtResponseDto.class);
        return response;
    }

    @Override
    public JwtResponseDto refresh(JwtResponseDto jwtResponseDto) throws ParseException {
        String token = jwtProvider.refreshToken(jwtResponseDto);
        return new JwtResponseDto(token);
    }


}
