package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestauranteResponseDto;

import java.util.List;

public interface IRestauranteHandler {
    void saveRestaurante(RestauranteRequestDto restauranteRequestDto);
    RestauranteResponseDto getRestauranteid(Long id);
    List<RestauranteResponseDto> getAllRestaurant();
    List<RestauranteResponseDto> getRestauranteOwner(String iduserpr);

}
