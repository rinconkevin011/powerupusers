package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
@AllArgsConstructor
@Getter
public class RestauranteResponseDto {
    private Long id;
    private String name;
    private String address;
    private String phone;
    private String urllogo;
    private String nit;
    private Long iduserPr;

}
