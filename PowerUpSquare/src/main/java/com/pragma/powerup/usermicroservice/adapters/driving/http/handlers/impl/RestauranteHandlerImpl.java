package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRestauranteHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IRestauranteRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IRestauranteResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.IRestauranteServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RestauranteHandlerImpl implements IRestauranteHandler {
    private final IRestauranteServicePort restauranteServicePort;
    private final IRestauranteRequestMapper restauranteRequestMapper;
    private final IRestauranteResponseMapper restauranteResponseMapper;

    @Override
    public void saveRestaurante(RestauranteRequestDto restauranteRequestDto) {
        restauranteServicePort.saveRestaurante(restauranteRequestMapper.toUser(restauranteRequestDto));
    }

    @Override
    public RestauranteResponseDto getRestauranteid(Long id) {
        return restauranteResponseMapper.userToPersonResponse(restauranteServicePort.getrestauranteid(id));
    }

    @Override
    public List<RestauranteResponseDto> getRestauranteOwner(String iduserpr) {
        return restauranteResponseMapper.toResponseList(restauranteServicePort.getrestauranteOwner(iduserpr));
    }

    @Override
    public List<RestauranteResponseDto> getAllRestaurant() {
        return restauranteResponseMapper.toResponseList(restauranteServicePort.getAllRestaurant());
    }
}
