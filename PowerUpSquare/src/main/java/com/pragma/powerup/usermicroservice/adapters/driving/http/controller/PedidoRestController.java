package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.*;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoplatoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPedidoHandler;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController()
@RequestMapping("/pedido")
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
public class PedidoRestController {
    private final IPedidoHandler pedidoHandler;
    @Operation(summary = "Add a new pedido",
            responses = {
                    @ApiResponse(responseCode = "201", description = "pedido created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "pedido already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Role not allowed for pedido creation",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/New")
    public ResponseEntity<Map<String, String>> saveUser(@RequestBody PedidoRequestDto pedidoRequestDto) {

        pedidoHandler.savePedido(pedidoRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.USER_CREATED_MESSAGE));
    }


    @Operation(summary = "Get pedido por estado (pendiente, preparacion, finalizado)",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Restaurant user returned",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = PedidoResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Restaurant not found with client role",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/GetPedidoEstado/{id}")
    public ResponseEntity<List<PedidoResponseDto>> getPedidoEstado(@PathVariable String id) {
        return ResponseEntity.ok(pedidoHandler.getPedidoEstado(id));
    }
    @Operation(summary = "Update Estado del Pedido(preparacion, finalizado)",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Pedido Update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "plato already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Role not allowed for plato Update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PutMapping("/UpdateEstado")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> updateUser(@RequestBody UpdatePedidoRequestDto updatePedidoRequestDto) {

        pedidoHandler.updatePedido(updatePedidoRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.USER_CREATED_MESSAGE));
    }
    @Operation(summary = "cancel order",
            responses = {
                    @ApiResponse(responseCode = "201", description = "plato Update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "plato already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Role not allowed for plato Update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PutMapping("/cancel/{id}")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> cancelpedido(@PathVariable Long id) {
        pedidoHandler.cancelPedido(id);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.USER_CREATED_MESSAGE));
    }

    @Operation(summary = "Get my orders (CLIENT)",
            responses = {
                    @ApiResponse(responseCode = "200", description = "All roles returned",
                            content = @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = PedidoResponseDto.class)))),
                    @ApiResponse(responseCode = "404", description = "No data found",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/MyOrders")
    public ResponseEntity<List<PedidoResponseDto>> getAllOrders() {
        return ResponseEntity.ok(pedidoHandler.getMyOrders());
    }

    @Operation(summary = "Get my orders (OWNER)",
            responses = {
                    @ApiResponse(responseCode = "200", description = "All roles returned",
                            content = @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = PedidoplatoResponseDto.class)))),
                    @ApiResponse(responseCode = "404", description = "No data found",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/Orders")
    public ResponseEntity<List<PedidoplatoResponseDto>> getAllOrdersOwner() {
        return ResponseEntity.ok(pedidoHandler.getOrders());
    }


}
