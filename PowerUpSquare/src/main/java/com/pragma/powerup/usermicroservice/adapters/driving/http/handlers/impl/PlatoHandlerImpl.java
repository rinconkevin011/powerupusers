package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.EnablePlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PlatoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPlatoHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPlatoRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPlatoResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.IPlatoServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlatoHandlerImpl implements IPlatoHandler {
    private final IPlatoServicePort platoServicePort;
    private final IPlatoRequestMapper platoRequestMapper;
    private final IPlatoResponseMapper platoResponseMapper;

    @Override
    public void savePlato(PlatoRequestDto platoRequestDto) {
        platoServicePort.savePlato(platoRequestMapper.toPlato(platoRequestDto));
    }

    @Override
    public void updatePlato(UpdatePlatoRequestDto updatePlatoRequestDto) {
        platoServicePort.updatePlato(platoRequestMapper.toPlato2(updatePlatoRequestDto));
    }

    @Override
    public void enablePlato(Long id) {
        platoServicePort.enablePlato(id);
    }

    @Override
    public List<PlatoResponseDto> getPlatoidrestaurant(Long idrestaurante) {
        return platoResponseMapper.ListplatoToRestaurantResponse(platoServicePort.getPlatoidrestaurante(idrestaurante));
    }

    @Override
    public List<PlatoResponseDto> getPlatoidCategory(Long idCategory) {
        return platoResponseMapper.ListplatoToRestaurantResponse(platoServicePort.getPlatoidCategory(idCategory));
    }

    @Override
    public PlatoResponseDto getPlatoid(Long id) {
        return platoResponseMapper.platoToPersonResponse(platoServicePort.getPlatoid(id));
    }
}
