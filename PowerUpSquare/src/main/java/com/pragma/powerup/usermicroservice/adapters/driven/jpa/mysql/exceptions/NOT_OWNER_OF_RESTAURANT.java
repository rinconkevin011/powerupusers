package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class NOT_OWNER_OF_RESTAURANT extends RuntimeException{
    public NOT_OWNER_OF_RESTAURANT() {
        super();
    }
}
