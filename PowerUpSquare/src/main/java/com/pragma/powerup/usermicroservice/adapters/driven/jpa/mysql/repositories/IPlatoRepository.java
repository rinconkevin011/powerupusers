package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IPlatoRepository extends JpaRepository<PlatoEntity, Long> {
    boolean existsByUrlimagen(String urlimagen);
    Optional<PlatoEntity> findById(Long id);
    List<PlatoEntity>  findByidrestaurante(Long idrestaurante);
    PlatoEntity findByid(Long id);


}
