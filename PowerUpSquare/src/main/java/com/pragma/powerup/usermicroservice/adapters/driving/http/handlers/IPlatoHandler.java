package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.EnablePlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PlatoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.Plato;

import java.util.List;

public interface IPlatoHandler {
    void savePlato(PlatoRequestDto platoRequestDto);
    void updatePlato(UpdatePlatoRequestDto updatePlatoRequestDto);
    void enablePlato(Long id);
    List<PlatoResponseDto> getPlatoidrestaurant(Long idrestaurante);
    List<PlatoResponseDto> getPlatoidCategory(Long idCategory);
    PlatoResponseDto getPlatoid(Long id);


}
