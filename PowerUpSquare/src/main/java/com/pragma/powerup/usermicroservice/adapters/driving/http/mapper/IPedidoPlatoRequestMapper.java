package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePedidoRequestDto;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoPlatoRequestMapper {
    Pedido_platos toPedidoplato(PedidoRequestDto pedidoRequestDto);
    Pedido_platos toPedidoPlato(UpdatePedidoRequestDto updatePedidoRequestDto);
}
