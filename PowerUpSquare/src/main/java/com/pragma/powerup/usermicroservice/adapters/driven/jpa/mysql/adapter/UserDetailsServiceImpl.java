package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PrincipalUser;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import jakarta.transaction.Transactional;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserDetailsServiceImpl {

    public UserDetails loadUserByUsername(String documentID, String token) throws UsernameNotFoundException, FileNotFoundException {

        UserEntity user = new UserEntity();
        List<RoleEntity> roles = new ArrayList<>();
        File archivo = new File("token");
        try {
            PrintWriter salida = new PrintWriter(archivo);
            salida.println(token);
            salida.close();
        }catch (Exception e){
            System.out.println("** Exception: "+ e.getMessage());
        }

        try {
            //traer los roles
            String theUrl = "http://localhost:8090/role";
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<List<RoleEntity>> responseRol = restTemplate.exchange(theUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<RoleEntity>>() {
            });

            roles = responseRol.getBody();

            //traer los User
            String theUrl2 = "http://localhost:8090/user/GetUser/"+documentID;
            RestTemplate restTemplate2 = new RestTemplate();
            HttpHeaders headers2 = new HttpHeaders();
            headers2.setContentType(MediaType.APPLICATION_JSON);
            headers2.set("Authorization", "Bearer "+token);
            headers2.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity2 = new HttpEntity<String>("parameters", headers2);
            ResponseEntity<UserEntity> responseUser = restTemplate2.exchange(theUrl2, HttpMethod.GET, entity2, UserEntity.class);
            user = responseUser.getBody();

        }
        catch (Exception eek) {
            System.out.println("** Exception: "+ eek.getMessage());
        }

        return PrincipalUser.build(user, roles);

    }
}
