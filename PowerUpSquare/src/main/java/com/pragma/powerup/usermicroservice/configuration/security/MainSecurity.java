package com.pragma.powerup.usermicroservice.configuration.security;


import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtEntryPoint;
import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class MainSecurity {

    @Autowired
    JwtEntryPoint jwtEntryPoint;
    @Bean
    public JwtTokenFilter jwtTokenFilter() {
        return new JwtTokenFilter();
    }
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests(requests -> requests

                        .requestMatchers("/auth/login", "/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**", "/actuator/health").permitAll()
                        .requestMatchers("/restaurante/New").hasAuthority("ROL_ADMIN")
                        .requestMatchers("/plato/New").hasAuthority("ROL_PROPIETARIO")
                        .requestMatchers("/plato/Update").hasAuthority("ROL_PROPIETARIO")
                        .requestMatchers("/auth/login").permitAll()
                        .requestMatchers("/restaurante/GetRestaurantId/{id}").permitAll()
                        .requestMatchers("/restaurante/GetRestaurantOwner/{id}").permitAll()
                        .requestMatchers("/restaurante/GetAllRestaurant").permitAll()
                        .requestMatchers("/plato/GetPlatoIdRestaurant/{id}").permitAll()
                        .requestMatchers("/plato/EnableDisenable").hasAuthority("ROL_PROPIETARIO")
                        .requestMatchers("/pedido/New").hasAuthority("ROL_CLIENTE")
                        .requestMatchers("/pedido/GetPedidoEstado/{id}").hasAuthority("ROL_EMPLEADO")
                        .requestMatchers("/plato/GetPlatoId/{id}").permitAll()
                        .requestMatchers("/pedido/cancel/{id}").hasAuthority("ROL_CLIENTE")
                        .requestMatchers("/pedido/MyOrders").hasAuthority("ROL_CLIENTE")
                        .requestMatchers("/pedido/Orders").hasAuthority("ROL_PROPIETARIO")
                        .requestMatchers("/plato/GetPlatoIdCategory/{id}").permitAll()
                        .requestMatchers("/category").permitAll()

                        .anyRequest().authenticated()
                )
                .formLogin().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(jwtEntryPoint);
        http.addFilterBefore(jwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration authConfig) throws Exception {
        return authConfig.getAuthenticationManager();
    }
}
