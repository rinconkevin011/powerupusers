package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoPlatoEntity;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoPlatoEntityMapper {
    @Mapping(target = "pedidoEntity.id", source = "id_pedido")
    @Mapping(target = "platoEntity.id", source = "id_plato")
    PedidoPlatoEntity toEntity(Pedido_platos pedidoPlatos);
    @Mapping(target = "id_pedido", source = "pedidoEntity.id")
    @Mapping(target = "id_plato", source = "platoEntity.id")
    Pedido_platos toUser(PedidoPlatoEntity pedidoPlatoEntity);
    @Mapping(target = "id_pedido", source = "pedidoEntity.id")
    @Mapping(target = "id_plato", source = "platoEntity.id")
    List<Pedido_platos> toPedidoList(List<PedidoPlatoEntity> pedidoPlatos);
}
