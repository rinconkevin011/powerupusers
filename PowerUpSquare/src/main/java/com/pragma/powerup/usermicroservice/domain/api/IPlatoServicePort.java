package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.domain.model.Plato;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;

import java.util.List;

public interface IPlatoServicePort {
    void savePlato(Plato plato);
    void updatePlato(Plato plato);
    void enablePlato(Long id);;
    List<Plato> getPlatoidrestaurante(Long idrestaurante);
    List<Plato> getPlatoidCategory(Long idCategory);
    Plato getPlatoid(Long id);
}
