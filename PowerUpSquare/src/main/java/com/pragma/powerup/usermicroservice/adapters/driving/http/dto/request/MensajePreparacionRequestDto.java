package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class MensajePreparacionRequestDto {
    private String user;
    private String menssage;
    private String phone;

    public MensajePreparacionRequestDto(String user, String menssage, String phone) {
        this.user = user;
        this.menssage = menssage;
        this.phone = phone;
    }
    public MensajePreparacionRequestDto(){

    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMenssage() {
        return menssage;
    }

    public void setMenssage(String menssage) {
        this.menssage = menssage;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
