package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PedidoplatoResponseDto {
    private Long id;
    private String value;
    private Long id_pedido;
    private Long id_plato;
    private String datastart;
    private String datafinal;

}
