package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.itIsNotNumeric;
import com.pragma.powerup.usermicroservice.domain.api.IRestauranteServicePort;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import com.pragma.powerup.usermicroservice.domain.spi.IRestaurantePersistencePort;

import java.util.List;

public class RestauranteUseCase implements IRestauranteServicePort {
    private final IRestaurantePersistencePort restaurantePersistencePort;
    public RestauranteUseCase(IRestaurantePersistencePort restaurantePersistencePort){
        this.restaurantePersistencePort = restaurantePersistencePort;
    }

    @Override
    public void saveRestaurante(Restaurante restaurante) {
        if(!restaurante.getNit().matches("[0-9]+")){
            throw new itIsNotNumeric();
        }
        if(!restaurante.getPhone().matches("[0-9]+")){
            throw new itIsNotNumeric();
        }
        restaurantePersistencePort.saveRestaurante(restaurante);
    }

    @Override
    public Restaurante getrestauranteid(Long id) {
        return restaurantePersistencePort.getrestauranteid(id);
    }

    @Override
    public List<Restaurante> getrestauranteOwner(String iduserpr) {
        return restaurantePersistencePort.getrestauranteOwner(iduserpr);
    }

    @Override
    public List<Restaurante> getAllRestaurant() {
        return restaurantePersistencePort.getAllRestaurant();
    }
}
