package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.LoginRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRestauranteHandler;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController()
@RequestMapping("/restaurante")
@RequiredArgsConstructor
public class RestauranteRestController {
    private final IRestauranteHandler restauranteHandler;

    @Operation(summary = "Add a new Restaurante",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Restaurante created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "Restaurante already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Role not allowed for Restaurante creation",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @SecurityRequirement(name = "jwt")
    @PostMapping("/New")
    public ResponseEntity<Map<String, String>> saveUser(@RequestBody RestauranteRequestDto restauranteRequestDto) {

        restauranteHandler.saveRestaurante(restauranteRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.USER_CREATED_MESSAGE));
    }
    @Operation(summary = "Get a Restaurant for ID",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Restaurant user returned",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = RestauranteResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Restaurant not found with client role",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/GetRestaurantId/{id}")
    public ResponseEntity<RestauranteResponseDto> getRestauranteid(@PathVariable Long id) {
        return ResponseEntity.ok(restauranteHandler.getRestauranteid(id));
    }
    @Operation(summary = "Get a Restaurant for IdOwner",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Restaurant user returned",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = RestauranteResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Restaurant not found with client role",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/GetRestaurantOwner/{id}")
    public ResponseEntity<List<RestauranteResponseDto>> getRestauranteOwner(@PathVariable String id) {
        return ResponseEntity.ok(restauranteHandler.getRestauranteOwner(id));
    }

    @Operation(summary = "Get all the restaurant",
            responses = {
                    @ApiResponse(responseCode = "200", description = "All restaurant returned",
                            content = @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = RestauranteResponseDto.class)))),
                    @ApiResponse(responseCode = "404", description = "No data found",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/GetAllRestaurant")
    public ResponseEntity<List<RestauranteResponseDto>> getAllRestaurantes() {
        return ResponseEntity.ok(restauranteHandler.getAllRestaurant());
    }


}
