package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.CategoryResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.ICategoryHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.ICategoryResponseMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPlatoRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPlatoResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.ICategoryServicePort;
import com.pragma.powerup.usermicroservice.domain.api.IPlatoServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class CategoryHandlerImpl implements ICategoryHandler {
    private final ICategoryResponseMapper categoryResponseMapper;
    private final ICategoryServicePort categoryServicePort;

    @Override
    public List<CategoryResponseDto> getAllCategory() {
        return categoryResponseMapper.toResponseList(categoryServicePort.getAllCategory());
    }
}
