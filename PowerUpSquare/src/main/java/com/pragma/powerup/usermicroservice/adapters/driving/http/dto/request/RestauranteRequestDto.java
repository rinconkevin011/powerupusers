package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RestauranteRequestDto {
    private String name;
    private String address;
    private String phone;
    private String urllogo;
    private String nit;
    private Long iduserPr;
}
