package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PlatoResponseDto {
    private Long id;
    private String name;
    private String description;
    private Long value;
    private String urlimagen;
    private Boolean activo;
    private Long id_categoria;
    private Long idrestaurante;
}
