package com.pragma.powerup.usermicroservice.domain.model;

public class Pedido_platos {
    private Long id;
    private String value;
    private Long id_pedido;
    private Long id_plato;
    private String datastart;
    private String datafinal;

    public Pedido_platos(Long id, String value, Long id_pedido, Long id_plato, String datastart, String datafinal) {
        this.id = id;
        this.value = value;
        this.id_pedido = id_pedido;
        this.id_plato = id_plato;
        this.datastart = datastart;
        this.datafinal = datafinal;
    }

    public Pedido_platos(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(Long id_pedido) {
        this.id_pedido = id_pedido;
    }

    public Long getId_plato() {
        return id_plato;
    }

    public void setId_plato(Long id_plato) {
        this.id_plato = id_plato;
    }

    public String getDatastart() {
        return datastart;
    }

    public void setDatastart(String datastart) {
        this.datastart = datastart;
    }

    public String getDatafinal() {
        return datafinal;
    }

    public void setDatafinal(String datafinal) {
        this.datafinal = datafinal;
    }
}
