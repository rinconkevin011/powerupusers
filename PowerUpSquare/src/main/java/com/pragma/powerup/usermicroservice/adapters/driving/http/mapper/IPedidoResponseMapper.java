package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoResponseMapper {
    @Mapping(source = "pedido.description", target = "description")
    @Mapping(source = "pedido.data", target = "data")
    @Mapping(source = "idrestaurante", target = "idrestaurante")
    @Mapping(source = "pedido.idusercl", target = "idusercl")
    @Mapping(source = "pedido.iduserch", target = "iduserch")
    PedidoResponseDto pedidoToPersonResponse(Pedido pedido);

    @Mapping(source = "pedido.description", target = "description")
    @Mapping(source = "pedido.data", target = "data")
    @Mapping(source = "idrestaurante", target = "idrestaurante")
    @Mapping(source = "pedido.idusercl", target = "idusercl")
    @Mapping(source = "pedido.iduserch", target = "iduserch")
    List<PedidoResponseDto> listpedidoToPersonResponse(List<Pedido> pedido);

}
