package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.Empleado;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.MensajePreparacionRequestDto;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtProvider;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class OtherTasks {
    private JwtProvider jwtProvider = new JwtProvider();
    public UserEntity GetUserDni (String dni){
        UserEntity user = new UserEntity();

        String theUrl2 = "http://localhost:8090/user/GetUser/"+dni;
        RestTemplate restTemplate2 = new RestTemplate();
        HttpHeaders headers2 = new HttpHeaders();
        headers2.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity2 = new HttpEntity<String>("parameters", headers2);
        ResponseEntity<UserEntity> responseUser = restTemplate2.exchange(theUrl2, HttpMethod.GET, entity2, UserEntity.class);
        user = responseUser.getBody();

        return user;
    }
    public String GetDniToken(){
        File archivo = new File("token");
        String token = "";

        try {
            //traer token
            BufferedReader entrada = new BufferedReader(new FileReader(archivo));
            token = entrada.readLine();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        archivo.delete();
        return token;
    }
    public Empleado GetEmployeeId(Long idEmployee){
        Empleado empleado = new Empleado();

        String theUrl = "http://localhost:8090/user/GetEmployeeId/"+idEmployee;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<Empleado> responseempleado = restTemplate.exchange(theUrl, HttpMethod.GET, entity, Empleado.class);
        empleado = responseempleado.getBody();

        return empleado;
    }
    public UserEntity GetUserId(Long idCliente){
        UserEntity user2 = new UserEntity();

        String theUrl3 = "http://localhost:8090/user/GetUserId/"+idCliente;
        RestTemplate restTemplate3 = new RestTemplate();
        HttpHeaders headers3 = new HttpHeaders();
        headers3.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity3 = new HttpEntity<String>("parameters", headers3);
        ResponseEntity<UserEntity> responseUser3 = restTemplate3.exchange(theUrl3, HttpMethod.GET, entity3, UserEntity.class);
        user2 = responseUser3.getBody();

        return user2;
    }
    public Long GetUserIdRestaurant(Long idCliente){
        UserEntity user2 = new UserEntity();

        String theUrl2 = "http://localhost:8090/user/GetUserId/"+idCliente;
        RestTemplate restTemplate2 = new RestTemplate();
        HttpHeaders headers2 = new HttpHeaders();
        headers2.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity2 = new HttpEntity<String>("parameters", headers2);
        ResponseEntity<String> responseUser = restTemplate2.exchange(theUrl2, HttpMethod.GET, entity2, String.class);
        String p = responseUser.getBody().toString();
        String[] a = p.split(":");
        String myStr = a[a.length-1].replace("}", "");
        Long i = Long.valueOf(myStr) ;

        return i;
    }
    public String CreateJson (MensajePreparacionRequestDto mensaje){
        String json = "";
        try {
            json = new ObjectMapper().writeValueAsString(mensaje);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return json;
    }
    public void sendMensage(String json){
        String theUrl4 = "http://localhost:8092/mensaje/preparacion";
        RestTemplate restTemplate4 = new RestTemplate();
        HttpHeaders headers4 = new HttpHeaders();
        headers4.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity4 = new HttpEntity(json, headers4);
        restTemplate4.exchange(theUrl4, HttpMethod.POST, entity4, String.class);
    }
    public String GetDataTime(){
        DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm:ss z");

        String date = dateFormat.format(new Date());

        return date;
    }
    public String GetData(){
        Calendar c = Calendar.getInstance();
        String dia, mes, año;

        dia = Integer.toString(c.get(Calendar.DATE));
        mes = Integer.toString(c.get(Calendar.MONTH));
        año = Integer.toString(c.get(Calendar.YEAR));
        String data = año+"-"+mes+"-"+dia;

        return data;
    }
}
