package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter.OtherTasks;
import com.pragma.powerup.usermicroservice.domain.api.IPedidoServicePort;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import com.pragma.powerup.usermicroservice.domain.spi.IPedidoPersistencePort;
import com.pragma.powerup.usermicroservice.domain.spi.IPlatoPersistencePort;

import java.util.List;

public class PedidoUseCase implements IPedidoServicePort {
    private final IPedidoPersistencePort pedidoPersistencePort;
    private OtherTasks otherTasks = new OtherTasks();
    public PedidoUseCase(IPedidoPersistencePort pedidoPersistencePort){
        this.pedidoPersistencePort = pedidoPersistencePort;
    }
    @Override
    public void savePedido(Pedido pedido, Pedido_platos pedidoPlatos) {
        pedido.setDescription(pedido.getDescription() + " - Id plato: "+pedidoPlatos.getId_plato());
        pedido.setData(otherTasks.GetData());
        pedidoPersistencePort.savePedido(pedido, pedidoPlatos);
    }

    @Override
    public void updatePedido(Pedido_platos pedidoPlatos) {
        pedidoPersistencePort.updatePedido(pedidoPlatos);
    }

    @Override
    public List<Pedido> getPedidosEstado(String value) {
        return pedidoPersistencePort.getPedidoEstado(value);
    }

    @Override
    public void cancelPedido(Long id) {
        pedidoPersistencePort.cancelPedido(id);
    }

    @Override
    public List<Pedido> getAllRoles() {
        return pedidoPersistencePort.getMyOrders();
    }

    @Override
    public List<Pedido_platos> getAllOrders() {
        return pedidoPersistencePort.getOrders();
    }
}
