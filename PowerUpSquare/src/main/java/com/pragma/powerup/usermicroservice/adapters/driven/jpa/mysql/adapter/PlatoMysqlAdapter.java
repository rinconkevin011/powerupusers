package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.nimbusds.jose.shaded.gson.JsonObject;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.MailAlreadyExistsException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.NOT_OWNER_OF_RESTAURANT;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.NoDataFoundException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IPlatoEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IPlatoRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IRestauranteRepository;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.JwtResponseDto;
import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtProvider;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import com.pragma.powerup.usermicroservice.domain.spi.IPlatoPersistencePort;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
public class PlatoMysqlAdapter implements IPlatoPersistencePort {
    private final IPlatoRepository platoRepository;
    private final IPlatoEntityMapper iplatoEntityMapper;
    private final IRestauranteRepository restauranteRepository;
    private final JwtProvider jwtProvider;
    private  OtherTasks otherTasks = new OtherTasks();
    @Override
    public void savePlato(Plato plato) {
        RestauranteEntity restaurante = restauranteRepository.findByid(plato.getIdrestaurante());
        String Token = otherTasks.GetDniToken();
        String dni = jwtProvider.getNombreUsuarioFromToken(Token);
        UserEntity user = otherTasks.GetUserDni(dni);

        if(Long.valueOf(restaurante.getIduserPr())  != user.getId()){
            throw new NOT_OWNER_OF_RESTAURANT();
        }
        platoRepository.save(iplatoEntityMapper.toEntity(plato));
    }

    @Override
    public void updatePlato(Plato plato) {
        Optional<PlatoEntity> plato1 = platoRepository.findById(plato.getId());
        PlatoEntity UpdatePlato = plato1.get();
        plato.setName(UpdatePlato.getName());
        plato.setId_categoria(UpdatePlato.getCategoriaEntity().getId());
        plato.setIdrestaurante(UpdatePlato.getIdrestaurante().getId());
        plato.setUrlimagen(UpdatePlato.getUrlimagen());
        plato.setActivo(UpdatePlato.getActivo());

        platoRepository.save(iplatoEntityMapper.toEntity(plato));
    }

    @Override
    public void enablePlato(Long id) {
        Optional<PlatoEntity> plato1 = platoRepository.findById(id);
        String Token = otherTasks.GetDniToken();
        String dni = jwtProvider.getNombreUsuarioFromToken(Token);

        UserEntity user = otherTasks.GetUserDni(dni);

        if(user.getId() != Long.valueOf(plato1.get().getIdrestaurante().getIduserPr())){
            throw new NOT_OWNER_OF_RESTAURANT();
        }
        PlatoEntity plato = plato1.get();
        if(plato.getActivo().equals(true)){
            plato.setActivo(false);
        }else {
            plato.setActivo(true);
        }

        platoRepository.save(plato);
    }

    @Override
    public List<Plato>  getPlatoidrestaurante(Long idrestaurante) {
        List<PlatoEntity> platoEntities = new ArrayList<>();
        List<PlatoEntity> platoEntity = platoRepository.findAll();
        if (platoEntity.isEmpty()) {
            throw new NoDataFoundException();
        }
        for (PlatoEntity plato : platoEntity){
            if(plato.getIdrestaurante().getId().equals(idrestaurante)){
                platoEntities.add(plato);
            }
        }
        return iplatoEntityMapper.toPlatoList(platoEntities);

    }

    @Override
    public List<Plato> getPlatoidCategory(Long idCategory) {
        List<PlatoEntity> platoEntities = new ArrayList<>();
        List<PlatoEntity> platoEntity = platoRepository.findAll();
        if (platoEntity.isEmpty()) {
            throw new NoDataFoundException();
        }
        for (PlatoEntity plato : platoEntity){
            if(plato.getCategoriaEntity().getId().equals(idCategory)){
                platoEntities.add(plato);
            }
        }
        return iplatoEntityMapper.toPlatoList(platoEntities);
    }

    @Override
    public Plato getPlatoid(Long id) {
        PlatoEntity plato = platoRepository.findByid(id);
        return iplatoEntityMapper.toUser(plato);
    }
}
