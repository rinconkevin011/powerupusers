package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.*;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.*;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IPedidoEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IPedidoPlatoEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IPedidoRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IPedido_platosRepository;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.MensajePreparacionRequestDto;
import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtProvider;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import com.pragma.powerup.usermicroservice.domain.spi.IPedidoPersistencePort;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
public class PedidoMysqlAdapter implements IPedidoPersistencePort {
    private final IPedidoRepository pedidoRepository;
    private final IPedidoEntityMapper pedidoEntityMapper;
    private final IPedido_platosRepository pedidoPlatosRepository;
    private final IPedidoPlatoEntityMapper pedidoPlatoEntityMapper;
    private final JwtProvider jwtProvider;
    private  OtherTasks otherTasks = new OtherTasks();
    @Override
    public void savePedido(Pedido pedido, Pedido_platos pedidoPlatos) {
        String Token = otherTasks.GetDniToken();
        String dni = jwtProvider.getNombreUsuarioFromToken(Token);
        UserEntity user = otherTasks.GetUserDni(dni);

        pedido.setIdusercl(user.getId());
        pedidoRepository.save(pedidoEntityMapper.toEntity(pedido));

        List<PedidoEntity> listp = pedidoRepository.findAll();
        Long p = listp.get(listp.size()-1).getId();
        pedidoPlatos.setId_pedido(p);
        pedidoPlatos.setValue("pendiente");

        pedidoPlatosRepository.save(pedidoPlatoEntityMapper.toEntity(pedidoPlatos));
    }

    @Override
    public void updatePedido(Pedido_platos pedidoPlatos) {
        Optional<PedidoPlatoEntity> pedido1 = pedidoPlatosRepository.findById(pedidoPlatos.getId());
        PedidoPlatoEntity updatePedido = pedido1.get();
        pedidoPlatos.setId_plato(updatePedido.getPlatoEntity().getId());
        pedidoPlatos.setId_pedido(updatePedido.getPedidoEntity().getId());
        if(pedidoPlatos.getValue().equals("preparacion")){
            pedidoPlatos.setDatastart(otherTasks.GetDataTime());
            pedidoPlatos.setDatafinal(updatePedido.getDatafinal());
        }else if(pedidoPlatos.getValue().equals("finalizado")){
            pedidoPlatos.setDatafinal(otherTasks.GetDataTime());
            pedidoPlatos.setDatastart(updatePedido.getDatastart() );
        }

        PedidoEntity pedido = pedidoRepository.findByid(pedidoPlatos.getId());
        String Token = otherTasks.GetDniToken();
        String dni = jwtProvider.getNombreUsuarioFromToken(Token);
        UserEntity user = otherTasks.GetUserDni(dni);
        Empleado empleado = otherTasks.GetEmployeeId(user.getId());

        if(empleado.getId_restaurante() != pedido.getRestauranteEntity().getId()){
            throw new NOT_OWNER_OF_RESTAURANT();
        }

        pedidoPlatosRepository.save(pedidoPlatoEntityMapper.toEntity(pedidoPlatos));
        pedido.setIduserch(user.getId());

        UserEntity user2 = otherTasks.GetUserId(pedido.getIdusercl());
        //user2.getPhone();
        MensajePreparacionRequestDto mensaje = new MensajePreparacionRequestDto(user2.getName(), pedidoPlatos.getValue(),"+573165692168" );

        String json = otherTasks.CreateJson(mensaje);

        otherTasks.sendMensage(json);

        pedidoRepository.save(pedido);
    }

    @Override
    public List<Pedido> getPedidoEstado(String value) {
        String Token = otherTasks.GetDniToken();
        String dni = jwtProvider.getNombreUsuarioFromToken(Token);
        UserEntity user =  otherTasks.GetUserDni(dni);
        Empleado empleado = otherTasks.GetEmployeeId(user.getId());

        List<PedidoEntity> pedidoEntities = new ArrayList<>();
        List<PedidoPlatoEntity> pedidoplatoEntities = pedidoPlatosRepository.findByvalue(value);
        if (pedidoplatoEntities.isEmpty()) {
            throw new NoDataFoundException();
        }

        for (PedidoPlatoEntity plato : pedidoplatoEntities){
            PedidoEntity PE = pedidoRepository.findByid(plato.getPedidoEntity().getId());
            if(empleado.getId_restaurante() == PE.getRestauranteEntity().getId()){
                pedidoEntities.add(PE);
            }
        }

        return pedidoEntityMapper.toPedidoList(pedidoEntities);
    }

    @Override
    public void cancelPedido(Long id) {
        PedidoEntity pedido = pedidoRepository.findByid(id);
        String Token = otherTasks.GetDniToken();
        String dni = jwtProvider.getNombreUsuarioFromToken(Token);
        UserEntity user = otherTasks.GetUserDni(dni);

        if(!pedido.getIdusercl().equals(user.getId())){
            throw new NOT_Client_OF_Order();
        }

        PedidoPlatoEntity pedidoPlatoEntity = pedidoPlatosRepository.findByid(id);

        if(!pedidoPlatoEntity.getValue().equals("pendiente")){
            throw new OrderIsNotPendig();
        }

        pedidoPlatosRepository.delete(pedidoPlatoEntity);
        pedidoRepository.delete(pedido);

    }

    @Override
    public List<Pedido> getMyOrders() {
        String Token = otherTasks.GetDniToken();
        String dni = jwtProvider.getNombreUsuarioFromToken(Token);
        UserEntity user = otherTasks.GetUserDni(dni);

        List<PedidoEntity> pedidoEntities2 = new ArrayList<>();
        List<PedidoEntity> pedidoEntities = pedidoRepository.findAll();

        if (pedidoEntities.isEmpty()) {
            throw new NoDataFoundException();
        }
        for(PedidoEntity p : pedidoEntities){
            if(user.getId().equals(p.getIdusercl())){
                pedidoEntities2.add(p);
            }
        }
        return pedidoEntityMapper.toPedidoList(pedidoEntities2);
    }

    @Override
    public List<Pedido_platos> getOrders() {
        String Token = otherTasks.GetDniToken();
        String dni = jwtProvider.getNombreUsuarioFromToken(Token);
        UserEntity user = otherTasks.GetUserDni(dni);

        List<PedidoPlatoEntity> pedidoEntities2 = new ArrayList<>();
        List<PedidoPlatoEntity> pedidoEntities = pedidoPlatosRepository.findAll();

        if (pedidoEntities.isEmpty()) {
            throw new NoDataFoundException();
        }

        for(PedidoPlatoEntity p : pedidoEntities){
            if(user.getId() == Long.valueOf(p.getPedidoEntity().getRestauranteEntity().getIduserPr())){
                pedidoEntities2.add(p);
            }
        }

        return pedidoPlatoEntityMapper.toPedidoList(pedidoEntities2);
    }

}
