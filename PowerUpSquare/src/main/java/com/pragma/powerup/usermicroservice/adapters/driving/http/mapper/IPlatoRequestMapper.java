package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.EnablePlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePlatoRequestDto;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPlatoRequestMapper {
    Plato toPlato(PlatoRequestDto platoRequestDto);
    Plato toPlato2(UpdatePlatoRequestDto updatePlatoRequestDto);
    Plato toPlato3(EnablePlatoRequestDto enablePlatoRequestDto);
}
