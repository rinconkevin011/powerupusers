package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pedido_platos")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PedidoPlatoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    private String value;
    @ManyToOne
    @JoinColumn(name = "id_pedido")
    private PedidoEntity pedidoEntity;
    @ManyToOne
    @JoinColumn(name = "id_plato")
    private PlatoEntity platoEntity;
    private String datastart;
    private String datafinal;
}
