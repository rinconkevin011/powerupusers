package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IRestauranteRepository extends JpaRepository<RestauranteEntity, Long> {
    RestauranteEntity findByid(Long id);
    List<RestauranteEntity> findByiduserPr(String iduserpr);
    Optional<RestauranteEntity> findByname(String name);
    boolean existsByUrllogo(String urllogo);
}
