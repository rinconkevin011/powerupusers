package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "plato")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PlatoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    private String name;
    private String description;
    private Long value;
    private String urlimagen;
    private Boolean activo;
    @ManyToOne
    @JoinColumn(name = "id_categoria")
    private CategoriaEntity categoriaEntity;
    @ManyToOne
    @JoinColumn(name = "idrestaurante")
    private RestauranteEntity idrestaurante;

}
