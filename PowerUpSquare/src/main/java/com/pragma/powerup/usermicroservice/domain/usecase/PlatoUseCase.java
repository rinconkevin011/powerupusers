package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.api.IPlatoServicePort;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import com.pragma.powerup.usermicroservice.domain.spi.IPlatoPersistencePort;

import java.util.List;

public class PlatoUseCase implements IPlatoServicePort {
    private final IPlatoPersistencePort platoPersistencePort;
    public PlatoUseCase(IPlatoPersistencePort platoPersistencePort){
        this.platoPersistencePort = platoPersistencePort;
    }
    @Override
    public void savePlato(Plato plato) {
        plato.setActivo(true);
        platoPersistencePort.savePlato(plato);
    }

    @Override
    public void updatePlato(Plato plato) {
        platoPersistencePort.updatePlato(plato);
    }

    @Override
    public void enablePlato(Long id) {
        platoPersistencePort.enablePlato(id);
    }

    @Override
    public List<Plato> getPlatoidrestaurante(Long idrestaurante) {
        return platoPersistencePort.getPlatoidrestaurante(idrestaurante);
    }

    @Override
    public List<Plato> getPlatoidCategory(Long idCategory) {
        return platoPersistencePort.getPlatoidCategory(idCategory);
    }

    @Override
    public Plato getPlatoid(Long id) {
        return platoPersistencePort.getPlatoid(id);
    }
}
