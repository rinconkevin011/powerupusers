package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.CategoriaEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoPlatoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICategoryeRepository extends JpaRepository<CategoriaEntity, Long> {
}
