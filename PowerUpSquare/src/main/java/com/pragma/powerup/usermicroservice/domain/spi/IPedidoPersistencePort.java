package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;

import java.util.List;

public interface IPedidoPersistencePort {
    void savePedido(Pedido pedido, Pedido_platos pedidoPlatos);
    void updatePedido(Pedido_platos pedidoPlatos);
    List<Pedido> getPedidoEstado(String value);
    void cancelPedido(Long id);
    List<Pedido> getMyOrders();
    List<Pedido_platos> getOrders();
}
