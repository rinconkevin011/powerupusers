package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePedidoRequestDto;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoRequestMapper {
    Pedido toPedido(PedidoRequestDto pedidoRequestDto);

}
