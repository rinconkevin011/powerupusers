package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IRestauranteResponseMapper {
    @Mapping(source = "restaurante.name", target = "name")
    @Mapping(source = "restaurante.address", target = "address")
    @Mapping(source = "restaurante.phone", target = "phone")
    @Mapping(source = "restaurante.urllogo", target = "urllogo")
    @Mapping(source = "restaurante.nit", target = "nit")
    @Mapping(source = "restaurante.iduserPr", target = "iduserPr")
    RestauranteResponseDto userToPersonResponse(Restaurante restaurante);
    @Mapping(source = "restaurante.name", target = "name")
    @Mapping(source = "restaurante.address", target = "address")
    @Mapping(source = "restaurante.phone", target = "phone")
    @Mapping(source = "restaurante.urllogo", target = "urllogo")
    @Mapping(source = "restaurante.nit", target = "nit")
    @Mapping(source = "restaurante.iduserPr", target = "iduserPr")
    List<RestauranteResponseDto> toResponseList(List<Restaurante> roleList);
}
