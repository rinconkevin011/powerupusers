package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.*;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IRestauranteEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IRestauranteRepository;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtProvider;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import com.pragma.powerup.usermicroservice.domain.spi.IRestaurantePersistencePort;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@RequiredArgsConstructor
@Transactional
public class RestauranteMysqlAdapter implements IRestaurantePersistencePort {
    private final IRestauranteRepository restauranteRepository;
    private final IRestauranteEntityMapper iRestauranteEntityMapper;
    private  OtherTasks otherTasks = new OtherTasks();
    @Override
    public void saveRestaurante(Restaurante restaurante) {
        Long role = otherTasks.GetUserIdRestaurant(restaurante.getIduserPr());

        if(role !=  Constants.OWNER_ROLE_ID){
            throw new ROL_NOT_ALLOWED();
        }
        if (restauranteRepository.findByname(restaurante.getName()).isPresent()){
            throw new thisnamealreadyexists();
        }
        if (restauranteRepository.existsByUrllogo(restaurante.getUrllogo())) {
            throw new thislogoalreadyexists();
        }
        restauranteRepository.save(iRestauranteEntityMapper.toEntity(restaurante));
    }

    @Override
    public Restaurante getrestauranteid(Long id) {
        RestauranteEntity restauranteEntity = restauranteRepository.findByid(id);
        return iRestauranteEntityMapper.toUser(restauranteEntity);
    }

    @Override
    public List<Restaurante> getrestauranteOwner(String iduserpr) {
        List<RestauranteEntity> restauranteEntitieslist = restauranteRepository.findByiduserPr(iduserpr);
        if (restauranteEntitieslist.isEmpty()) {
            throw new NoDataFoundException();
        }
        return iRestauranteEntityMapper.toRoleList(restauranteEntitieslist);
    }

    @Override
    public List<Restaurante> getAllRestaurant() {
        List<RestauranteEntity> restauranteEntitieslist = restauranteRepository.findAll();
        if (restauranteEntitieslist.isEmpty()) {
            throw new NoDataFoundException();
        }
        return iRestauranteEntityMapper.toRoleList(restauranteEntitieslist);
    }

}
