package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers
        ;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePedidoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoplatoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestauranteResponseDto;

import java.util.List;


public interface IPedidoHandler {
    void savePedido(PedidoRequestDto pedidoRequestDto);
    void updatePedido(UpdatePedidoRequestDto updatePedidoRequestDto);
    List<PedidoResponseDto> getPedidoEstado(String value);
    void cancelPedido(Long id);
    List<PedidoResponseDto> getMyOrders();
    List<PedidoplatoResponseDto> getOrders();
}
