package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoPlatoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IPedido_platosRepository extends JpaRepository<PedidoPlatoEntity, Long> {
    List<PedidoPlatoEntity> findByvalue(String value);
    PedidoPlatoEntity findByid(Long id);
}
