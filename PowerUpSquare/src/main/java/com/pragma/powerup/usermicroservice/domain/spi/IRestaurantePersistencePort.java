package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.domain.model.Restaurante;

import java.util.List;

public interface IRestaurantePersistencePort {
    void saveRestaurante(Restaurante restaurante);
    Restaurante getrestauranteid(Long id);
    List<Restaurante> getrestauranteOwner(String iduserpr);

    List<Restaurante> getAllRestaurant();
}
