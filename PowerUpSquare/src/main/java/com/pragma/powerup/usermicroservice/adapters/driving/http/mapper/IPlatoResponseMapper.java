package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PlatoResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPlatoResponseMapper {
    @Mapping(source = "plato.name", target = "name")
    @Mapping(source = "plato.description", target = "description")
    @Mapping(source = "plato.value", target = "value")
    @Mapping(source = "plato.urlimagen", target = "urlimagen")
    @Mapping(source = "plato.activo", target = "activo")
    @Mapping(source = "id_categoria", target = "id_categoria")
    @Mapping(source = "idrestaurante", target = "idrestaurante")
    PlatoResponseDto platoToPersonResponse(Plato plato);

    @Mapping(source = "plato.name", target = "name")
    @Mapping(source = "plato.description", target = "description")
    @Mapping(source = "plato.value", target = "value")
    @Mapping(source = "plato.urlimagen", target = "urlimagen")
    @Mapping(source = "plato.activo", target = "activo")
    @Mapping(source = "id_categoria", target = "id_categoria")
    @Mapping(source = "idrestaurante", target = "idrestaurante")
    List<PlatoResponseDto> ListplatoToRestaurantResponse(List<Plato> plato);
}
