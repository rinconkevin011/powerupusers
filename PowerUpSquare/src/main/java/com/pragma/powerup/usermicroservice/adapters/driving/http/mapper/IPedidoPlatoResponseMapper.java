package com.pragma.powerup.usermicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoplatoResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoPlatoResponseMapper {
    PedidoplatoResponseDto pedidoToPersonResponse(Pedido_platos pedidoPlatos);
    List<PedidoplatoResponseDto> listpedidoToPersonResponse(List<Pedido_platos> pedidoPlatos);
}
