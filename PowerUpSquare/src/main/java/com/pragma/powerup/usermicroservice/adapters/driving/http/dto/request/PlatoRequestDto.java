package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PlatoRequestDto {
    private String name;
    private String description;
    private Long value;
    private String urlimagen;
    private Long id_categoria;
    private Long idrestaurante;
}
