package com.pragma.powerup.usermicroservice.configuration;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter.*;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.*;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.*;
import com.pragma.powerup.usermicroservice.configuration.security.jwt.JwtProvider;
import com.pragma.powerup.usermicroservice.domain.api.ICategoryServicePort;
import com.pragma.powerup.usermicroservice.domain.api.IPedidoServicePort;
import com.pragma.powerup.usermicroservice.domain.api.IPlatoServicePort;
import com.pragma.powerup.usermicroservice.domain.api.IRestauranteServicePort;
import com.pragma.powerup.usermicroservice.domain.spi.ICategoryPersistencePort;
import com.pragma.powerup.usermicroservice.domain.spi.IPedidoPersistencePort;
import com.pragma.powerup.usermicroservice.domain.spi.IPlatoPersistencePort;
import com.pragma.powerup.usermicroservice.domain.spi.IRestaurantePersistencePort;
import com.pragma.powerup.usermicroservice.domain.usecase.CategoryUseCase;
import com.pragma.powerup.usermicroservice.domain.usecase.PedidoUseCase;
import com.pragma.powerup.usermicroservice.domain.usecase.PlatoUseCase;
import com.pragma.powerup.usermicroservice.domain.usecase.RestauranteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
    private final IRestauranteRepository restauranteRepository;
    private final IRestauranteEntityMapper restauranteEntityMapperr;
    private final ICategoryeRepository categoryeRepository;
    private final ICategoryEntityMapper categoryEntityMapper;
    private final IPlatoRepository platoRepository;
    private final IPlatoEntityMapper platoEntityMapperr;
    private final IPedidoRepository pedidoRepository;
    private final IPedidoEntityMapper pedidoEntityMapper;
    private final IPedido_platosRepository pedidoPlatosRepository;
    private final IPedidoPlatoEntityMapper pedidoPlatoEntityMapper;
    private final JwtProvider jwtProvider;

    @Bean
    public IRestauranteServicePort userServicePort() {
        return new RestauranteUseCase(restaurantePersistencePort());
    }
    @Bean
    public IRestaurantePersistencePort restaurantePersistencePort() {
        return new RestauranteMysqlAdapter(restauranteRepository,  restauranteEntityMapperr);
    }
    @Bean
    public IPlatoServicePort platoServicePort() {
        return new PlatoUseCase(platoPersistencePort());
    }
    @Bean
    public IPlatoPersistencePort platoPersistencePort() {
        return new PlatoMysqlAdapter(platoRepository,  platoEntityMapperr, restauranteRepository, jwtProvider);
    }
    @Bean
    public IPedidoServicePort pedidoServicePort() {
        return new PedidoUseCase(pedidoPersistencePort());
    }
    @Bean
    public IPedidoPersistencePort pedidoPersistencePort() {
        return new PedidoMysqlAdapter(pedidoRepository,  pedidoEntityMapper,  pedidoPlatosRepository, pedidoPlatoEntityMapper, jwtProvider);
    }
    @Bean
    public ICategoryServicePort categoryServicePort() {
        return new CategoryUseCase(categoryPersistencePort());
    }
    @Bean
    public ICategoryPersistencePort categoryPersistencePort() {
        return new CategoryMysqlAdapter(categoryeRepository,  categoryEntityMapper);
    }
}
