package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoEntityMapper {
    @Mapping(target = "restauranteEntity.id", source = "idrestaurante")
    PedidoEntity toEntity(Pedido pedido);
    @Mapping(target = "idrestaurante", source = "restauranteEntity.id")
    Pedido toUser(PedidoEntity pedidoEntity);
    @Mapping(target = "idrestaurante", source = "restauranteEntity.id")
    List<Pedido> toPedidoList(List<PedidoEntity> pedidoEntities);
}
