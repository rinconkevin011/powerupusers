package com.pragma.powerup.usermicroservice.domain.model;

public class Restaurante {
    private Long id;
    private String name;
    private String address;
    private String phone;
    private String urllogo;
    private String nit;
    private Long iduserPr;

    public Restaurante(Long id, String name, String address, String phone, String urllogo, String nit, Long iduserPr) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.urllogo = urllogo;
        this.nit = nit;
        this.iduserPr = iduserPr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUrllogo() {
        return urllogo;
    }

    public void setUrllogo(String urllogo) {
        this.urllogo = urllogo;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public Long getIduserPr() {
        return iduserPr;
    }

    public void setIduserPr(Long iduserPr) {
        this.iduserPr = iduserPr;
    }
}
