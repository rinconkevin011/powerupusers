package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PedidoRequestDto {
    private String description;
    private Long idrestaurante;
    private Long id_plato;
}
