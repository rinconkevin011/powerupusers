package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PedidoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePedidoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PedidoplatoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPedidoHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPedidoPlatoRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPedidoPlatoResponseMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPedidoRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IPedidoResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.IPedidoServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PedidoHandlerImpl implements IPedidoHandler {
    private final IPedidoServicePort pedidoServicePort;
    private final IPedidoRequestMapper pedidoRequestMapper;
    private final IPedidoPlatoRequestMapper pedidoPlatoRequestMapper;
    private final IPedidoPlatoResponseMapper pedidoPlatoResponseMapper;
    private final IPedidoResponseMapper pedidoResponseMapper;

    @Override
    public void savePedido(PedidoRequestDto pedidoRequestDto) {
        pedidoServicePort.savePedido(pedidoRequestMapper.toPedido(pedidoRequestDto), pedidoPlatoRequestMapper.toPedidoplato(pedidoRequestDto));
    }

    @Override
    public void updatePedido(UpdatePedidoRequestDto updatePedidoRequestDto) {
        pedidoServicePort.updatePedido(pedidoPlatoRequestMapper.toPedidoPlato(updatePedidoRequestDto));
    }

    @Override
    public List<PedidoResponseDto> getPedidoEstado(String value) {
        return pedidoResponseMapper.listpedidoToPersonResponse(pedidoServicePort.getPedidosEstado(value));
    }

    @Override
    public void cancelPedido(Long id) {
        pedidoServicePort.cancelPedido(id);
    }

    @Override
    public List<PedidoResponseDto> getMyOrders() {
        return pedidoResponseMapper.listpedidoToPersonResponse(pedidoServicePort.getAllRoles());
    }

    @Override
    public List<PedidoplatoResponseDto> getOrders() {
        return pedidoPlatoResponseMapper.listpedidoToPersonResponse(pedidoServicePort.getAllOrders());
    }
}
