package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "pedido")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PedidoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    private String description;
    private String data;
    @ManyToOne
    @JoinColumn(name = "idrestaurante")
    private RestauranteEntity restauranteEntity;
    private Long idusercl;
    private Long iduserch;

}
