package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PedidoResponseDto {
    private Long id;
    private String description;
    private String data;
    private Long idrestaurante;
    private Long idusercl;
    private Long iduserch;
}
