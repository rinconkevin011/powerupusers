package com.pragma.powerup.usermicroservice.domain.model;

import lombok.Data;

public class Pedido {
    private Long id;
    private String description;
    private String data;
    private Long idrestaurante;
    private Long idusercl;
    private Long iduserch;

    public Pedido(Long id, String description, String data, Long idrestaurante, Long idusercl, Long iduserch) {
        this.id = id;
        this.description = description;
        this.data = data;
        this.idrestaurante = idrestaurante;
        this.idusercl = idusercl;
        this.iduserch = iduserch;
    }
    public Pedido(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getIdrestaurante() {
        return idrestaurante;
    }

    public void setIdrestaurante(Long idrestaurante) {
        this.idrestaurante = idrestaurante;
    }

    public Long getIdusercl() {
        return idusercl;
    }

    public void setIdusercl(Long idusercl) {
        this.idusercl = idusercl;
    }

    public Long getIduserch() {
        return iduserch;
    }

    public void setIduserch(Long iduserch) {
        this.iduserch = iduserch;
    }
}
