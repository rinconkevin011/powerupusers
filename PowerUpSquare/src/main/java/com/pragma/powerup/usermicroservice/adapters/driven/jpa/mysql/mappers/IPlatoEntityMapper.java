package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPlatoEntityMapper {
    @Mapping(target = "idrestaurante.id", source = "idrestaurante")
    @Mapping(target = "categoriaEntity.id", source = "id_categoria")
    PlatoEntity toEntity(Plato plato);
    @Mapping(target = "idrestaurante", source = "idrestaurante.id")
    @Mapping(target = "id_categoria", source = "categoriaEntity.id")
    Plato toUser(PlatoEntity platoEntity);
    @Mapping(target = "idrestaurante", source = "idrestaurante.id")
    @Mapping(target = "id_categoria", source = "categoriaEntity.id")
    List<Plato> toPlatoList(List<PlatoEntity> platoEntities);
}
