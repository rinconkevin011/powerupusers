package com.pragma.powerup.usermicroservice.adapters.driving.http.controller;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.EnablePlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UpdatePlatoRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PlatoResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IPlatoHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IRestauranteHandler;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController()
@RequestMapping("/plato")
@RequiredArgsConstructor
public class PlatoRestController {
    private final IPlatoHandler platoHandler;

    @Operation(summary = "Add a new plato",
            responses = {
                    @ApiResponse(responseCode = "201", description = "plato created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "plato already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Role not allowed for plato creation",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/New")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> saveUser(@RequestBody PlatoRequestDto platoRequestDto) {

        platoHandler.savePlato(platoRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.USER_CREATED_MESSAGE));
    }

    @Operation(summary = "Update a plato",
            responses = {
                    @ApiResponse(responseCode = "201", description = "plato Update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "plato already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Role not allowed for plato Update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PutMapping("/Update")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> updateUser(@RequestBody UpdatePlatoRequestDto updatePlatoRequestDto) {

        platoHandler.updatePlato(updatePlatoRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.USER_CREATED_MESSAGE));
    }

    @Operation(summary = "enable or disenable a plato",
            responses = {
                    @ApiResponse(responseCode = "201", description = "plato Update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "plato already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Role not allowed for plato Update",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PutMapping("/EnableDisenable/{id}")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> enablePlato(@PathVariable Long id){

        platoHandler.enablePlato(id);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.USER_CREATED_MESSAGE));
    }
    @Operation(summary = "Get a Plato for Restaurant",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Restaurant user returned",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = RestauranteResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Restaurant not found with client role",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/GetPlatoIdRestaurant/{id}")
    public ResponseEntity<List<PlatoResponseDto>> getPlatoRestaurante(@PathVariable Long id) {
        return ResponseEntity.ok(platoHandler.getPlatoidrestaurant(id));
    }
    @Operation(summary = "Get a Plato for Category",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Plato user returned",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = RestauranteResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Plato not found with client role",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/GetPlatoIdCategory/{id}")
    public ResponseEntity<List<PlatoResponseDto>> getPlatoCategory(@PathVariable Long id) {
        return ResponseEntity.ok(platoHandler.getPlatoidCategory(id));
    }

    @Operation(summary = "Get a Plato for Restaurant",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Restaurant user returned",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = RestauranteResponseDto.class))),
                    @ApiResponse(responseCode = "404", description = "Restaurant not found with client role",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/GetPlatoId/{id}")
    public ResponseEntity<PlatoResponseDto> getPlatoId(@PathVariable Long id) {
        return ResponseEntity.ok(platoHandler.getPlatoid(id));
    }



}
