package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.domain.model.Pedido;
import com.pragma.powerup.usermicroservice.domain.model.Pedido_platos;
import com.pragma.powerup.usermicroservice.domain.model.Plato;
import com.pragma.powerup.usermicroservice.domain.model.Restaurante;

import java.util.List;

public interface IPedidoServicePort {
    void savePedido(Pedido pedido, Pedido_platos pedidoPlatos);
    void updatePedido(Pedido_platos pedidoPlatos);
    List<Pedido> getPedidosEstado(String value);
    void cancelPedido(Long id);
    List<Pedido> getAllRoles();
    List<Pedido_platos> getAllOrders();
}
